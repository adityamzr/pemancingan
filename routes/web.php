<?php

use App\Http\Controllers\BarangController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\IkanController;
use App\Http\Controllers\KeranjangController;
use App\Http\Controllers\KolamController;
use App\Http\Controllers\KolamSewaController;
use App\Http\Controllers\LaporanBarangController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MitraController;
use App\Http\Controllers\PenyewaanController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\UserController;
use App\Models\KolamSewa;
use App\Http\Controllers\HistoryController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.dashboard.index');
// });

Route::get('/', [LoginController::class, 'index'])->name('login');
Route::post('/', [LoginController::class, 'authenticate']);
Route::get('/logout', [LoginController::class, 'logout']);

Route::middleware('auth')->group(function(){
    // ====================== DASHBOARD ======================
    Route::get('/dashboard', [DashboardController::class, 'index']);

    // ====================== USER ======================
    Route::get('/user', [UserController::class, 'index']);
    Route::post('/user/getData', [UserController::class, 'getAllData'])->name('user.get');
    Route::get('/user/form', [UserController::class, 'create']);
    Route::post('/user', [UserController::class, 'store']);
    Route::get('/user/{id}/edit', [UserController::class, 'edit']);
    Route::patch('/user/{id}', [UserController::class, 'update']);
    Route::post('/user/{id}', [UserController::class, 'destroy']);

    // ====================== IKAN ======================
    Route::get('/ikan', [IkanController::class, 'index']);
    Route::post('/ikan/getData', [IkanController::class, 'getAllData'])->name('ikan.get');
    Route::get('/ikan/form', [IkanController::class, 'create']);
    Route::post('/ikan', [IkanController::class, 'store']);
    Route::get('/ikan/{id}/edit', [IkanController::class, 'edit']);
    Route::patch('/ikan/{id}', [IkanController::class, 'update']);
    Route::post('/ikan/{id}', [IkanController::class, 'destroy']);
    Route::get('/ikan/export_excel', [IkanController::class, 'export']);

    // ====================== KOLAM ======================
    Route::get('/kolam', [KolamController::class, 'index']);
    Route::post('/kolam/getData', [KolamController::class, 'getAllData'])->name('kolam.get');
    Route::get('/kolam/form', [KolamController::class, 'create']);
    Route::get('/kolam/{id}', [KolamController::class, 'show']);
    Route::post('/kolam', [KolamController::class, 'store']);
    Route::post('/kolam/storeIkan', [KolamController::class, 'storeIkan'])->name('storeIkan.post');
    Route::get('/kolam/{id}/edit', [KolamController::class, 'edit']);
    Route::patch('/kolam/{id}', [KolamController::class, 'update']);
    Route::post('/kolam/{id}', [KolamController::class, 'destroy']);
    // Route::get('/kolam/{id}', [KolamController::class, 'getStokIkan'])->name('getOneIkan.get');
    Route::post('/kolam/resetKolam/{id}', [KolamController::class, 'resetKolam']);

    // ====================== KOLAM SEWA ======================
    Route::resource('sewa-kolam', KolamSewaController::class)->except('destroy');
    Route::post('/sewa-kolam/getData', [KolamSewaController::class, 'getAllData'])->name('sewa-kolam.get');
    Route::post('/sewa-kolam/{id}', [KolamSewaController::class, 'destroy']);

    // ====================== BARANG ======================
    Route::get('/barang', [BarangController::class, 'index']);
    Route::post('/barang/getData', [BarangController::class, 'getAllData'])->name('barang.get');
    Route::get('/barang/form', [BarangController::class, 'create']);
    Route::post('/barang', [BarangController::class, 'store']);
    Route::get('/barang/{id}/edit', [BarangController::class, 'edit']);
    Route::post('/barang/{id}/edit/getData', [HistoryController::class, 'getHistoryBarang'])->name('history-barang');
    Route::patch('/barang/{id}', [BarangController::class, 'update']);
    Route::post('/barang/{id}', [BarangController::class, 'destroy']);
    Route::get('/barang/export_excel', [BarangController::class, 'export']);

    // ====================== TRANSAKSI ======================
    Route::get('/transaksi', [TransaksiController::class, 'index']);
    Route::post('/transaksi/getData', [TransaksiController::class, 'getAllData'])->name('transaksi.get');
    Route::post('/transaksi', [TransaksiController::class, 'store'])->name('storeBayar.post');
    // Route::get('/barang/form', [TransaksiController::class, 'create']);
    // Route::get('/barang/{id}/edit', [TransaksiController::class, 'edit']);
    // Route::patch('/barang/{id}', [TransaksiController::class, 'update']);
    // Route::post('/barang/{id}', [TransaksiController::class, 'destroy']);

    // ====================== KERANJANG ======================
    Route::post('/transaksi/storeItem', [KeranjangController::class, 'store'])->name('storeItem.post');
    Route::post('/transaksi/qtyUp/{id}', [KeranjangController::class, 'up']);
    Route::post('/transaksi/qtyDown/{id}', [KeranjangController::class, 'down']);
    Route::post('/transaksi/removeItem/{id}', [KeranjangController::class, 'destroy']);

    // ====================== LAPORAN BARANG ======================
    Route::resource('laporanbarang', LaporanBarangController::class);

    // ====================== PENYEWAAN ======================
    Route::get('/penyewaan/export_excel', [PenyewaanController::class, 'export']);
    Route::resource('penyewaan', PenyewaanController::class);
    Route::post('/penyewaan/getData', [PenyewaanController::class, 'getAllData'])->name('penyewaan.get');
    Route::post('penyewaann', [PenyewaanController::class, 'selesai'])->name('selesai');


    // ====================== Mitra ======================
    Route::resource('mitra', MitraController::class)->except('destroy');
    Route::post('/mitra/getData', [MitraController::class, 'getAllData'])->name('mitra.get');
    Route::post('/mitra/{id}', [MitraController::class, 'destroy']);

     // ====================== Pengurangan ======================
     Route::get('pengurangan-ikan', [HistoryController::class, 'PenguranganIkan']);
     Route::post('pengurangan-ikan/getData', [HistoryController::class, 'getPenguranganIkan'])->name('pengurangan-ikan.get');
});
