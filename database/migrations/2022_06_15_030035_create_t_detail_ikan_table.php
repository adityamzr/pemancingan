<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_detail_ikan', function (Blueprint $table) {
            $table->id('id_detail_ikan');
            $table->unsignedBigInteger('id_ikan');
            $table->foreign('id_ikan')->references('id_ikan')->on('t_ikan');
            $table->unsignedBigInteger('id_mitra');
            $table->foreign('id_mitra')->references('id_mitra')->on('t_mitra');
            $table->string('jenis_ikan', 100);
            $table->integer('jumlah_beli');
            $table->string('harga_beli');
            $table->date('tgl_pembelian');
            $table->string('bukti_pembelian')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_detail_ikan');
    }
};
