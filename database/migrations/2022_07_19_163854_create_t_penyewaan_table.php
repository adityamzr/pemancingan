<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_penyewaan', function (Blueprint $table) {
            $table->id('id_penyewaan');
            $table->unsignedBigInteger('id_kolam_sewa');
            $table->foreign('id_kolam_sewa')->references('id_kolam_sewa')->on('t_kolam_sewa');
            $table->string('nama_penyewa');
            $table->string('nohp', 20);
            $table->text('alamat');
            $table->date('tgl_sewa');
            $table->integer('tangkapan')->nullable();
            $table->string('status', 20);
            $table->integer('bayar')->nullable();
            $table->integer('kembali')->nullable();
            $table->integer('total_bayar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_penyewaan');
    }
};
