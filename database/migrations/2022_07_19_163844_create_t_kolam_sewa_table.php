<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_kolam_sewa', function (Blueprint $table) {
            $table->id('id_kolam_sewa');
            $table->string('nama_kolam_sewa', 100);
            $table->integer('kapasitas_kolam_sewa');
            $table->integer('status_kolam_sewa');
            $table->integer('isi_kolam_sewa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_kolam_sewa');
    }
};
