<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_history_pengurangan_ikan', function (Blueprint $table) {
            $table->id('id_pengurangan_ikan');
            $table->integer('asal');
            $table->integer('pengurangan');
            $table->integer('stok_sekarang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_history_pengurangan_ikan');
    }
};
