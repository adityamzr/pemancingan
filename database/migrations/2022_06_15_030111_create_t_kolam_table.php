<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_kolam', function (Blueprint $table) {
            $table->id('id_kolam');
            $table->integer('id_ikan');
            $table->string('nama_kolam', 100);
            $table->integer('kapasitas_kolam');
            $table->integer('status_kolam');
            $table->integer('isi_kolam');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_kolam');
    }
};
