<?php

namespace Database\Seeders;

use App\Models\Ikan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::insert ( [ 
            [
                'nama_user' => 'Pak Owner',
                'username' => 'admin',
                'password' => bcrypt('admin123'),
                'jabatan' => 'Owner',
                'role' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nama_user' => 'Pak Petugas',
                'username' => 'petugas',
                'password' => bcrypt('petugas123'),
                'jabatan' => 'Karyawan',
                'role' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);

        Ikan::create([
            'stok_ikan' => 0
        ]);
    }
}
