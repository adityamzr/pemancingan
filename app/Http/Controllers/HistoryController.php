<?php

namespace App\Http\Controllers;

use App\Models\HistoryBarang;
use App\Models\PenguranganIkan;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function PenguranganIkan()
    {
        return view('pages.pengurangan.ikan.index');
    }

    public function getPenguranganIkan(Request $request)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = PenguranganIkan::orderBy('id_pengurangan_ikan', 'DESC');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('asal', 'like', '%' . $search['value'] . '%');
                $query->orWhere('pengurangan', 'like', '%' . $search['value'] . '%');
                $query->orWhere('stok_sekarang', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;
            $d[] = $row->created_at->toDateString();
            $d[] = date('H:i', strtotime($row->created_at));
            $d[] = $row->asal;
            $d[] = $row->pengurangan;
            $d[] = $row->stok_sekarang;

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    public function getHistoryBarang(Request $request, $id)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = HistoryBarang::with('user')->where('id_barang', $id)->orderBy('id_history_barang', 'DESC');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('asal', 'like', '%' . $search['value'] . '%');
                $query->orWhere('perubahan', 'like', '%' . $search['value'] . '%');
                $query->orWhere('stok_sekarang', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $row->created_at->toDateString();
            $d[] = date('H:i', strtotime($row->created_at));
            if($row->id_user == 0){
                $d[] = "Pembeli";
            }else{
                $d[] = $row->user->nama_user;
            }
            $d[] = $row->asal;
            if ($row->type === 'pengurangan') {
                $d[] = 0;
                $d[] = $row->perubahan;
            } else {
                $d[] = $row->perubahan;
                $d[] = 0;
            }

            $d[] = $row->stok_sekarang;
            $d[] = $row->keterangan;

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }
}
