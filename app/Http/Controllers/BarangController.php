<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use App\Exports\LaporanBarangExport;
use App\Models\HistoryBarang;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['barang'] = Barang::all();
        return view('pages.barang.index');
    }

    public function export()
    {
        $tes = Carbon::now();
        $tes = $tes->toArray();
        $currentDate = $tes['day'].'-'.$tes['month'].'-'.$tes['year'];
        return Excel::download(new LaporanBarangExport, 'Laporan Barang '.$currentDate.'.xlsx');
    }

    public function getAllData(Request $request)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = Barang::orderBy('id_barang');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('nama_barang', 'like', '%' . $search['value'] . '%');
                $query->orWhere('harga_barang', 'like', '%' . $search['value'] . '%');
                $query->orWhere('jenis_barang', 'like', '%' . $search['value'] . '%');
                $query->orWhere('stok_barang', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;
            $d[] = $row->nama_barang;
            $d[] = $row->jenis_barang;
            $d[] = $row->harga_barang . ' / pcs';
            $d[] = $row->deskripsi_barang;
            $d[] = $row->stok_barang . ' / pcs';

            $btn = '<a href="'.url('barang/'.$row->id_barang.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-pencil"></i></a>';
            $btn .= '<button class="btn btn-danger btn-hapus" data-hapus-id="'.$row->id_barang.'" data-hapus-nama="'.$row->nama_barang.'"><i class="fas fa-trash"></i></button>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.barang.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $this->validate($request, [
            'nama_barang' => 'required',
            'jenis_barang' => 'required',
            'harga_barang' => 'required',
            'tgl_beli' => 'required',
            'deskripsi_barang' => '',
            'stok_barang' => 'required'
        ]);

        DB::beginTransaction();
        try{
            $barang = new Barang();
            $barang->nama_barang = $input['nama_barang'];
            $barang->jenis_barang = $input['jenis_barang'];
            $barang->harga_barang = $input['harga_barang'];
            $barang->tgl_beli = $input['tgl_beli'];
            if($input['deskripsi_barang'] == null){
                $barang->deskripsi_barang = '-';
            }else{
                $barang->deskripsi_barang = $input['deskripsi_barang'];
            }
            $barang->stok_barang = $input['stok_barang'];

            if($request->hasFile('bukti')){
                $barang->bukti = $request['bukti']->hashName();
                $request->bukti->store('foto_bukti', 'public');
            }

            $barang->save();

            DB::commit();

            return redirect('/barang')->with('success', 'Berhasil menambah data barang');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['barang'] = Barang::find($id);
        return view('pages.barang.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $this->validate($request, [
            'nama_barang' => 'required',
            'jenis_barang' => 'required',
            'harga_barang' => 'required',
            'tgl_beli' => 'required',
            'deskripsi_barang' => '',
            'stok_barang' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $barang = Barang::find($id);
            $asal = $barang->stok_barang;
            $barang->nama_barang = $input['nama_barang'];
            $barang->jenis_barang = $input['jenis_barang'];
            $barang->harga_barang = $input['harga_barang'];
            $barang->tgl_beli = $input['tgl_beli'];
            if(!empty($input['deskripsi_barang'])){
                $barang->deskripsi_barang = $input['deskripsi_barang'];
            }
            $barang->stok_barang = $input['stok_barang'];

            if($request->hasFile('bukti')){
                $path = public_path("storage/foto_bukti/$barang->bukti");
                if(File::exists($path)){
                    File::delete($path);
                }
                $barang->bukti = $request['bukti']->hashName();
                $request->bukti->store('foto_bukti', 'public');
            }

            $barang->save();

            if($request['keterangan'] != null){
                $id_user = Auth()->user()->id_user;
                $type = '';
                if($asal < $input['stok_barang']) {
//                    $perubahan = $input['stok_barang'] - $asal;
//                    $type = 'penambahan';
                    return redirect()->back()->with('error', 'Tidak dapat tambah barang');
                }else{
                    $perubahan = $asal - $input['stok_barang'];
                    $type = 'pengurangan';
                }
                HistoryBarang::create([
                    'id_barang' => $id,
                    'id_user' => $id_user,
                    'asal' => $asal,
                    'perubahan' => $perubahan,
                    'stok_sekarang' => $barang->stok_barang,
                    'keterangan' => $request['keterangan'],
                    'type' => $type
                ]);
            }

            DB::commit();

            return redirect('/barang')->with('success', 'Berhasil mengubah data barang');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::find($id);
        $path = public_path("storage/foto_bukti/$barang->bukti");

        $delete = $barang->delete();
        if($delete){
            if(File::exists($path)){
                File::delete($path);
            }

            return redirect()->back()->with('success', 'Berhasil menghapus data');
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data');
        }
    }
}
