<?php

namespace App\Http\Controllers;

use App\Models\KolamSewa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KolamSewaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.kolam-sewa.index');
    }

    public function getAllData(Request $request)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = KolamSewa::orderBy('id_kolam_sewa');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('nama_kolam_sewa', 'like', '%' . $search['value'] . '%');
                $query->orWhere('kapasitas_kolam_sewa', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;
            $d[] = $row->nama_kolam_sewa;
            $d[] = $row->kapasitas_kolam_sewa;
            if($row->status_kolam_sewa == 0){
                $d[] = "Sedang Tidak disewa";
            }else{
                $d[] = "Sedang disewa";
            }
            
            // $btn = '<a href="'.url('sewa-kolam/'.$row->id_kolam_sewa) .'" class="btn btn-info" data-id-kolam="'.$row->id_kolam_sewa.'"><i class="fas fa-eye"></i></a>';
            $btn = '<a href="'.url('sewa-kolam/'.$row->id_kolam_sewa.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-pencil"></i></a>';
            $btn .= '<button class="btn btn-danger btn-hapus" data-hapus-id="'.$row->id_kolam_sewa.'" data-hapus-nama="'.$row->nama_kolam_sewa.'"><i class="fas fa-trash"></i></button>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.kolam-sewa.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $this->validate($request, [
            'nama_kolam_sewa' => 'required',
            'kapasitas_kolam_sewa' => 'required'
        ]);
        
        DB::beginTransaction();
        try{
            $kolamSewa = new KolamSewa();
            $kolamSewa->nama_kolam_sewa = $input['nama_kolam_sewa'];
            $kolamSewa->kapasitas_kolam_sewa = $input['kapasitas_kolam_sewa'];
            $kolamSewa->status_kolam_sewa = 0;
            $kolamSewa->isi_kolam_sewa = 0;
            $kolamSewa->save();
            
            DB::commit();

            return redirect('/sewa-kolam')->with('success', 'Berhasil menambah data kolam');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages.kolam-sewa.detail');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kolam_sewa'] = KolamSewa::find($id);
        return view('pages.kolam-sewa.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $this->validate($request, [
            'nama_kolam_sewa' => 'required',
            'kapasitas_kolam_sewa' => 'required'
        ]);

        DB::beginTransaction();
        try{
            $kolamSewa = KolamSewa::find($id);
            $kolamSewa->nama_kolam_sewa = $input['nama_kolam_sewa'];
            $kolamSewa->kapasitas_kolam_sewa = $input['kapasitas_kolam_sewa'];
            $kolamSewa->save();

            DB::commit();

            return redirect('/sewa-kolam')->with('success', 'Berhasil mengubah data kolam');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kolamSewa = KolamSewa::find($id);
        $delete = $kolamSewa->delete();
        if($delete){
            return redirect()->back()->with('success', 'Berhasil menghapus data');
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data');
        }
    }
}
