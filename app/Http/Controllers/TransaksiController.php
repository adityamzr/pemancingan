<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\DetailTransaksi;
use App\Models\HistoryBarang;
use App\Models\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data['barang'] = Barang::all();
        return view('pages.transaksi.index');
    }

    public function getAllData(Request $request)
    {
        function titik($angka){
            $hasil_rupiah = "" . number_format($angka,0,'','.');

            return $hasil_rupiah;
        }

        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 5;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = Barang::orderBy('id_barang');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('nama_barang', 'like', '%' . $search['value'] . '%');
                $query->orWhere('harga_barang', 'like', '%' . $search['value'] . '%');
                $query->orWhere('jenis_barang', 'like', '%' . $search['value'] . '%');
                $query->orWhere('stok_barang', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        $getData = [];
        if(!empty($search) and !empty($search['value'])){
            $getData = $data_tmp->get();
        }
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ( $getData as $row) {
            $d = [];

            $d[] = $row->nama_barang;
            $d[] = $row->jenis_barang;
            $d[] = titik($row->harga_barang);
            $d[] = $row->deskripsi_barang;
            $d[] = $row->stok_barang;

            $btn = '<button class="btn btn-primary btn-pilih" data-id-barang="'.$row->id_barang.'" data-nama-barang="'.$row->nama_barang.'">Pilih</button>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $this->validate($request, [
            'total_bayar' => 'required',
            'bayar' => 'required',
            'kembali' => 'required'
        ]);

        $keranjang = session()->get('cart_items');
        $data['bayar'] = $input['bayar'];
        $data['kembali'] = $input['kembali'];

        $nomor = Carbon::now()->timestamp;

        DB::beginTransaction();
        try {
            $transaksi = new Transaksi();
            $transaksi->id_user = auth()->user()->id_user;
            $transaksi->nomor_transaksi = $nomor;
            $transaksi->total_bayar = $input['total_bayar'];
            $transaksi->save();

            $id_transaksi = $transaksi->id;

            foreach($keranjang as $item){
                $detailTransaksi = new DetailTransaksi();
                $detailTransaksi->id_transaksi = $transaksi->id;
                $detailTransaksi->id_barang = $item['barang']->id_barang;
                $detailTransaksi->qty = $item['qty'];
                $detailTransaksi->subtotal = $item['subtotal'];
                $detailTransaksi->save();

                $barang = Barang::find($item['barang']->id_barang);
                $asal = $barang->stok_barang;
                $barang->stok_barang = $barang->stok_barang - $item['qty'];
                $barang->save();

                HistoryBarang::create([
                    'id_barang' => $item['barang']->id_barang,
                    'id_user' => 0,
                    'asal' => $asal,
                    'perubahan' => $item['qty'],
                    'stok_sekarang' => $barang->stok_barang,
                    'keterangan' => "Dibeli",
                    'type' => "pengurangan",
                ]);
            }

            DB::commit();

            session()->forget('cart_items');

            $data['transaksi'] = Transaksi::where('id_transaksi', $id_transaksi)->first();
            $data['detail'] = DetailTransaksi::where('id_transaksi', $id_transaksi)->get();
            return view('pages.transaksi.struk', $data);

        }catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
