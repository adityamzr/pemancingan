<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user'] = User::all();
        return view('pages.user.index', $data);
    }

    public function getAllData(Request $request)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = User::orderBy('id_user');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('nama_user', 'like', '%' . $search['value'] . '%');
                $query->orWhere('email', 'like', '%' . $search['value'] . '%');
                $query->orWhere('jabatan', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;
            $d[] = $row->nama_user;
            $d[] = $row->username;
            $d[] = $row->jabatan;
            if($row->role == 0){
                $d[] = "Admin";
            }else{
                $d[] = "Petugas";
            }
        
            $btn = '<a href="'.url('user/'.$row->id_user.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-eye"></i></a>';
            $btn .= '<button class="btn btn-danger btn-hapus" data-hapus-id="'.$row->id_user.'" data-hapus-nama="'.$row->nama_user.'"><i class="fas fa-trash"></i></button>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $this->validate($request, [
            'nama_user' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jabatan' => 'required',
            'role' => 'required'
        ]);

        DB::beginTransaction();
        try{
            $user = new User;
            $user->nama_user = $input['nama_user'];
            $user->username = $input['username'];
            $user->password = bcrypt($input['password']);
            $user->jabatan = $input['jabatan'];
            $user->role = $input['role'];
            $user->save();

            DB::commit();

            return redirect('/user')->with('success', 'Berhasil menambah data user');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::find($id);
        return view('pages.user.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $this->validate($request, [
            'nama_user' => 'required',
            'username' => 'required',
            'password' => '',
            'jabatan' => 'required',
            'role' => 'required'
        ]);

        DB::beginTransaction();
        try{
            $user = User::find($id);
            $user->nama_user = $input['nama_user'];
            $user->username = $input['username'];
            if(!empty($input['password'])){
                $user->password = bcrypt($input['password']);
            }
            $user->jabatan = $input['jabatan'];
            $user->role = $input['role'];
            $user->save();

            DB::commit();

            return redirect('/user')->with('success', 'Berhasil mengubah data user');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $delete = $user->delete();
        if($delete){
            return redirect()->back()->with('success', 'Berhasil menghapus data');
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data');
        }
    }
}
