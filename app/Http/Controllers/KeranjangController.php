<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class KeranjangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_barang' => 'required',
        ]);
        
        try{
            $barang = Barang::findOrFail($request->id_barang);
            $cart_items = session()->get('cart_items', []);
            $cart_total = 0;
            
            if(array_key_exists($request->id_barang, $cart_items)){
                $cart_items[$request->id_barang]['qty'] += 1;
                $cart_items[$request->id_barang]['subtotal'] += $barang->harga_barang;
            }else{
                $cart_items[$request->id_barang] = [
                    'barang' => $barang,
                    'qty' => 1,
                    'subtotal' => $barang->harga_barang * 1
                ];
            }
            // dd($cart_items);
                
            foreach($cart_items as $item){
                $cart_total += $item['subtotal'];
            }

            session()->put('cart_items', $cart_items);
            session()->put('cart_total', $cart_total);

            return response()->json([
                'data' => [
                    'cart_items' => view('pages.transaksi.cart_items')->render(),
                    'cart_total' => 'Rp '.number_format($cart_total)
                ],
            ]);
        }catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function up($id)
    {
        try{
            $barang = Barang::findOrFail($id);
            $cart_items = session()->get('cart_items', []);
            $cart_total = 0;
            
            if(array_key_exists($id, $cart_items)){
                $cart_items[$id]['qty'] += 1;
                $cart_items[$id]['subtotal'] += $barang->harga_barang;
            }
                
            foreach($cart_items as $item){
                $cart_total += $item['subtotal'];
            }

            session()->put('cart_items', $cart_items);
            session()->put('cart_total', $cart_total);

            return response()->json([
                'data' => [
                    'cart_items' => view('pages.transaksi.cart_items')->render(),
                    'cart_total' => 'Rp '.number_format($cart_total)
                ],
            ]);
        }catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function down($id)
    {
        try{
            $barang = Barang::findOrFail($id);
            $cart_items = session()->get('cart_items', []);
            $cart_total = session()->get('cart_total');
            
            if(array_key_exists($id, $cart_items)){
                $cart_items[$id]['qty'] -= 1;
                $cart_items[$id]['subtotal'] -= $barang->harga_barang;
            }
                
            foreach($cart_items as $item){
                $cart_total -= $item['barang']->harga_barang;
            }

            session()->put('cart_items', $cart_items);
            session()->put('cart_total', $cart_total);

            return response()->json([
                'data' => [
                    'cart_items' => view('pages.transaksi.cart_items')->render(),
                    'cart_total' => 'Rp '.number_format($cart_total)
                ],
            ]);
        }catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'qty' => 'required',
        ]);

        try{
            $cart_items = session()->get('cart_items', []);
            $cart_total = 0;

            if(array_key_exists($id, $cart_items)){
                $cart_items[$id]['qty'] = $request->qty;
            }

            foreach($cart_items as $item){
                $cart_total += $item['produk']->harga * $item['qty'];
            }

            session()->put('cart_items', $cart_items);
            session()->put('cart_total', $cart_total);

            return response()->json([
                'data' => [
                    'cart_items' => view('pages.transaksi.cart_items')->render(),
                    'cart_total' => 'Rp '.number_format($cart_total)
                ],
            ]);
        }catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $cart_items = session()->get('cart_items', []);
            $cart_total = 0;

            if(array_key_exists($id, $cart_items)){
                unset($cart_items[$id]);
            }

            foreach($cart_items as $item){
                $cart_total += $item['subtotal'];
            }

            session()->put('cart_items', $cart_items);
            session()->put('cart_total', $cart_total);

            return response()->json([
                'data' => [
                    'cart_items' => view('pages.transaksi.cart_items')->render(),
                    'cart_total' => 'Rp '.number_format($cart_total)
                ],
            ]);
        }catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
