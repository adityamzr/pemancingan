<?php

namespace App\Http\Controllers;

use App\Models\Ikan;
use App\Models\Kolam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KolamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['ikan'] = Ikan::where('stok_ikan', '>=', 1)->get();
        $data['kolam'] = Kolam::all();
        return view('pages.kolam.index', $data);
    }

    public function getStokIkan($id)
    {
        $data['ikan'] = Ikan::where('id_ikan', $id)->first();
        return response()->json($data);
    }

    public function getAllData(Request $request)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = Kolam::orderBy('id_kolam');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('nama_kolam', 'like', '%' . $search['value'] . '%');
                $query->orWhere('kapasitas_kolam', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;
            $d[] = $row->nama_kolam;
            $d[] = $row->kapasitas_kolam;
            if($row->status_kolam == 1){
                $d[] = $row->isi_kolam;
            }else{
                $d[] = 'Kosong';
            }

            $btn = '<a href="'.url('kolam/'.$row->id_kolam) .'" class="btn btn-info" data-id-kolam="'.$row->id_kolam.'"><i class="fas fa-eye"></i></a>';
            $btn .= '<a href="'.url('kolam/'.$row->id_kolam.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-pencil"></i></a>';
            $btn .= '<button class="btn btn-danger btn-hapus" data-hapus-id="'.$row->id_kolam.'" data-hapus-nama="'.$row->nama_kolam.'"><i class="fas fa-trash"></i></button>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.kolam.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $this->validate($request, [
            'nama_kolam' => 'required',
            'kapasitas_kolam' => 'required'
        ]);

        DB::beginTransaction();
        try{
            $kolam = new Kolam();
            $kolam->id_ikan = 0;
            $kolam->nama_kolam = $input['nama_kolam'];
            $kolam->kapasitas_kolam = $input['kapasitas_kolam'];
            $kolam->status_kolam = 0;
            $kolam->isi_kolam = 0;

            // dd($kolam);
            $kolam->save();


            DB::commit();

            return redirect('/kolam')->with('success', 'Berhasil menambah data kolam');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    public function storeIkan(Request $request){
        $input = $request->all();
        DB::beginTransaction();
        try {
            $ikan = Ikan::find(1);
            $ikan->stok_ikan = $ikan->stok_ikan - $input['stok_ikan'];
            $ikan->save();

            $kolam = Kolam::find($input['id']);
            $kolam->id_ikan = 1;
            $kolam->status_kolam = 1;
            $kolam->isi_kolam = $kolam->isi_kolam + $input['stok_ikan'];
            $kolam->save();

            DB::commit();

        } catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['kolam'] = Kolam::find($id);
        $data['ikan'] = Ikan::find(1);
        return view('pages.kolam.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kolam'] = Kolam::find($id);
        return view('pages.kolam.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $this->validate($request, [
            'nama_kolam' => 'required',
            'kapasitas_kolam' => 'required'
        ]);

        DB::beginTransaction();
        try{
            $kolam = Kolam::find($id);
            $kolam->nama_kolam = $input['nama_kolam'];
            $kolam->kapasitas_kolam = $input['kapasitas_kolam'];
            $kolam->save();

            DB::commit();

            return redirect('/kolam')->with('success', 'Berhasil mengubah data kolam');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kolam = Kolam::find($id);
        $delete = $kolam->delete();
        if($delete){
            return redirect()->back()->with('success', 'Berhasil menghapus data');
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data');
        }
    }

    public function resetKolam($id)
    {
        $kolam = Kolam::find($id);
        $resetKolam = $kolam->update([
            'id_ikan' => 0,
            'status_kolam' => 0,
            'isi_kolam' => 0
        ]);

        if($resetKolam){
            return redirect()->back()->with('success', 'Berhasil mengosongkan kolam');
        }else{
            return redirect()->back()->with('error', 'Gagal mengosongkan kolam');
        }
    }
}
