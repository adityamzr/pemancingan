<?php

namespace App\Http\Controllers;

use App\Models\Ikan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rules\Exists;

use App\Exports\LaporanIkanExport;
use App\Models\DetailIkan;
use App\Models\Mitra;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class IkanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['ikan'] = DetailIkan::all();
        return view('pages.ikan.index');
    }

    public function export()
    {
        $tes = Carbon::now();
        $tes = $tes->toArray();
        $currentDate = $tes['day'].'-'.$tes['month'].'-'.$tes['year'];
        return Excel::download(new LaporanIkanExport, 'Laporan Ikan '.$currentDate.'.xlsx');
    }

    public function getAllData(Request $request)
    {
        function titik($angka){
            $hasil_rupiah = "" . number_format($angka,0,'','.');

            return $hasil_rupiah;
        }

        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = DetailIkan::with('mitra')->orderBy('id_detail_ikan', 'DESC');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('tgl_pembelian', 'like', '%' . $search['value'] . '%');
                $query->orWhere('harga_beli', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;
            $d[] = $row->jenis_ikan;
            $d[] = $row->jumlah_beli;
            $d[] = $row->jumlah_beli_kg;
            $d[] = $row->mitra->nama_mitra;
            $d[] = $row->mitra->alamat;
            $d[] = $row->mitra->kontak;
            $d[] = 'Rp'.titik($row->harga_beli);

            $btn = '<a href="'.url('ikan/'.$row->id_detail_ikan.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-pencil"></i></a>';
            $btn .= '<button class="btn btn-danger btn-hapus" data-hapus-id="'.$row->id_detail_ikan.'" data-hapus-nama="'.$row->jenis_ikan.'"><i class="fas fa-trash"></i></button>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['mitra'] = Mitra::all();
        return view('pages.ikan.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $this->validate($request, [
            'id_mitra' => 'required',
            'jenis_ikan' => 'required',
            'jumlah_beli' => 'required',
            'jumlah_beli_kg' => 'required',
            'harga_beli' => 'required',
            'tgl_pembelian' => 'required',
            'bukti_pembelian' => 'required'
        ]);

        DB::beginTransaction();
        try{
            $detail_ikan = new DetailIkan();
            $detail_ikan->id_ikan = 1;
            $detail_ikan->id_mitra = $input['id_mitra'];
            $detail_ikan->jenis_ikan = $input['jenis_ikan'];
            $detail_ikan->jumlah_beli = $input['jumlah_beli'];
            $detail_ikan->jumlah_beli_kg = $input['jumlah_beli_kg'];
            $detail_ikan->tgl_pembelian = $input['tgl_pembelian'];
            $detail_ikan->harga_beli = $input['harga_beli'];

            if($request->hasFile('bukti_pembelian')){
                $detail_ikan->bukti_pembelian = $request['bukti_pembelian']->hashName();
                $request->bukti_pembelian->store('foto_bukti', 'public');
            }

            $detail_ikan->save();

            $ikan = Ikan::find(1);
            $ikan->stok_ikan = $ikan->stok_ikan + $input['jumlah_beli'];
            $ikan->save();

            DB::commit();

            return redirect('/ikan')->with('success', 'Berhasil menambah data');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['detail_ikan'] = DetailIkan::find($id);
        $data['mitra'] = Mitra::all();
        return view('pages.ikan.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $this->validate($request, [
            'id_mitra' => 'required',
            'jenis_ikan' => 'required',
            'jumlah_beli' => 'required',
            'harga_beli' => 'required',
            'tgl_pembelian' => 'required'
        ]);

        DB::beginTransaction();
        try{
            $detail_ikan = DetailIkan::find($id);

            $ikan = Ikan::find(1);
            $ikan->stok_ikan = ($ikan->stok_ikan - $detail_ikan->jumlah_beli) + $input['jumlah_beli'];
            $ikan->save();

            $detail_ikan->id_ikan = 1;
            $detail_ikan->id_mitra = $input['id_mitra'];
            $detail_ikan->jenis_ikan = $input['jenis_ikan'];
            $detail_ikan->jumlah_beli = $input['jumlah_beli'];
            $detail_ikan->tgl_pembelian = $input['tgl_pembelian'];
            $detail_ikan->harga_beli = $input['harga_beli'];

            if($request->hasFile('bukti_pembelian')){
                $path = public_path("storage/foto_bukti/$detail_ikan->bukti_pembelian");
                if(File::exists($path)){
                    File::delete($path);
                }
                $detail_ikan->bukti_pembelian = $request['bukti_pembelian']->hashName();
                $request->bukti_pembelian->store('foto_bukti', 'public');
            }

            $detail_ikan->save();

            DB::commit();

            return redirect('/ikan')->with('success', 'Berhasil mengubah data');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('error', $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail_ikan = DetailIkan::find($id);
        $path = public_path("storage/foto_bukti/$detail_ikan->bukti_pembelian");

        $delete = $detail_ikan->delete();
        if($delete){
            if(File::exists($path)){
                File::delete($path);
            }

            return redirect()->back()->with('success', 'Berhasil menghapus data');
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data');
        }
    }
}
