<?php

namespace App\Http\Controllers;

use App\Models\Mitra;
use Illuminate\Http\Request;

class MitraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.mitra.index');
    }

    public function getAllData(Request $request)
    {
        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = Mitra::orderBy('id_mitra');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('nama_mitra', 'like', '%' . $search['value'] . '%');
                $query->orWhere('alamat', 'like', '%' . $search['value'] . '%');
                $query->orWhere('kontak', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;
            $d[] = $row->nama_mitra;
            $d[] = $row->alamat;
            $d[] = $row->kontak;
        
            $btn = '<a href="'.url('mitra/'.$row->id_mitra.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-pencil"></i></a>';
            $btn .= '<button class="btn btn-danger btn-hapus" data-hapus-id="'.$row->id_mitra.'" data-hapus-nama="'.$row->nama_mitra.'"><i class="fas fa-trash"></i></button>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.mitra.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'nama_mitra' => 'required',
            'kontak' => 'required',
            'alamat' => 'required'
        ]);

        $store = Mitra::create($input);

        if($store){
            return redirect('/mitra')->with('success', 'Berhasil menambah data');
        }else{
            return redirect()->back()->with('error', 'Gagal menambah data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['mitra'] = Mitra::find($id);
        return view('pages.mitra.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'nama_mitra' => 'required',
            'kontak' => 'required',
            'alamat' => 'required'
        ]);

        $mitra = Mitra::find($id);
        $update = $mitra->update($input);

        if($update){
            return redirect('/mitra')->with('success', 'Berhasil mengubah data');
        }else{
            return redirect()->back()->with('error', 'Gagal mengubah data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mitra = Mitra::find($id);

        $delete = $mitra->delete();
        if($delete){
            return redirect()->back()->with('success', 'Berhasil menghapus data');
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data');
        }
    }
}
