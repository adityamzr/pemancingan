<?php

namespace App\Http\Controllers;

use App\Exports\LaporanPenyewaanExport;
use App\Models\Kolam;
use App\Models\KolamSewa;
use App\Models\PenguranganIkan;
use App\Models\Penyewaan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PenyewaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data['penyewaan'] = Penyewaan::all();
        return view('pages.penyewaan.index', $data);
    }

    public function export()
    {
        $tes = Carbon::now();
        $tes = $tes->toArray();
        $currentDate = $tes['day'].'-'.$tes['month'].'-'.$tes['year'];
        return Excel::download(new LaporanPenyewaanExport(), 'Laporan Penyewaan '.$currentDate.'.xlsx');
    }


    public function getAllData(Request $request)
    {
        function titik($angka){
            $hasil_rupiah = "" . number_format($angka,0,'','.');

            return $hasil_rupiah;
        }

        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];
        $column = $order[0]['column'];

        $data = Penyewaan::with('kolam')->orderBy('id_penyewaan');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) and !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('nama_penyewa', 'like', '%' . $search['value'] . '%');
                $query->orWhere('nohp', 'like', '%' . $search['value'] . '%');
                $query->orWhere('alamat', 'like', '%' . $search['value'] . '%');
                $query->orWhere('status', 'like', '%' . $search['value'] . '%');
                $query->orWhere('tangkapan', 'like', '%' . $search['value'] . '%');
                $query->orWhere('tgl_sewa', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);
        // dd($data->get());
        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;
            $d[] = $row->tgl_sewa;
            $d[] = $row->kolam->nama_kolam_sewa;
            $d[] = $row->nama_penyewa;
            $d[] = $row->nohp;
            $d[] = $row->alamat;
            $d[] = $row->tangkapan;
            $d[] = "Rp".titik($row->total_bayar);
            $d[] = $row->status;

            if($row->status == 'Sewa'){
                $btn = '<button class="btn btn-primary btn-selesai" data-id="'.$row->id_penyewaan.'" data-nama="'.$row->nama_penyewa.'"><i class="fas fa-check"></i> Selesai</button>';
                $btn .= '<a href="'.url('penyewaan/'.$row->id_penyewaan.'/edit') .'" class="btn btn-warning mx-1"><i class="fas fa-eye"></i></a>';
                $btn .= '<button class="btn btn-danger btn-hapus" data-hapus-id="'.$row->id_penyewaan.'" data-hapus-nama="'.$row->nama_penyewa.'"><i class="fas fa-trash"></i></button>';
            }else{
                $btn = '';
            }

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);

            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kolam'] = KolamSewa::where('status_kolam_sewa', 0)->get();
        $data['stok'] = Kolam::get();
        $stok_ikan = 0;
//        dd($data['stok']);
        foreach($data['stok'] as $stok){
            $stok_ikan += $stok->isi_kolam;
        }
        $data['stok_ikan'] = $stok_ikan;
        return view('pages.penyewaan.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'tgl_sewa' => 'required',
            'stok' => 'required',
            'id_kolam_sewa' => 'required',
            'nama_penyewa' => 'required',
            'nohp' => 'required',
            'alamat' => 'required',
            'tangkapan' => 'required',
            'total_bayar' => 'required',
            'bayar' => 'required',
            'kembali' => 'required',
            'id_stok' => 'required'
        ]);

//        dd($input);

        if($input['tangkapan'] > $input['stok']) return redirect()->back()->with('error', 'Jumlah beli tidak boleh lebih dari stok ikan');
        if($input['bayar'] < $input['total_bayar']) return redirect()->back()->with('error', 'Uang dibayar kurang!');

        $kolam_sewa = KolamSewa::find($input['id_kolam_sewa']);
        if($input['tangkapan'] > $kolam_sewa->kapasitas_kolam_sewa) return redirect()->back()->with('error', 'Kapasitas kolam tidak cukup');

        $kolam_stok = Kolam::where(['id_kolam' => $input['id_stok']])->first();
//        dd($kolam_stok);
        $stok_ikan = $kolam_stok->isi_kolam;
//        foreach($kolam_stok as $stok){
//            $stok_ikan += $stok->isi_kolam;
//        }
        PenguranganIkan::create([
            'asal' => $stok_ikan,
            'pengurangan' => $input['tangkapan'],
            'stok_sekarang' => $stok_ikan - $input['tangkapan'],
        ]);

        $sisa = $input['tangkapan'];
        $terpenuhi = false;
        if($kolam_stok->isi_kolam != 0){
            $sisa -= $kolam_stok->isi_kolam;
            $isi = 0;
            if($sisa <= 0){
                $isi = $sisa * -1;
                $terpenuhi = true;
            }
            $kolam_stok->update(['isi_kolam' => $isi,'status_kolam' => $isi != 0 ? 1 : 0]);
        }
//        foreach($kolam_stok as $item){
//            if($item->isi_kolam != 0){
//                $sisa -= $item->isi_kolam;
//                $isi = 0;
//                if($sisa <= 0){
//                    $isi = $sisa * -1;
//                    $terpenuhi = true;
//                }
//                $item->update(['isi_kolam' => $isi,'status_kolam' => $isi != 0 ? 1 : 0]);
//            }
//            if($terpenuhi) break;
//        }

        $input['status'] = "Sewa";
        $store = Penyewaan::create($input);
        $kolam_sewa->update([
            'status_kolam_sewa' => 1,
            'isi_kolam_sewa' => $input['tangkapan']
        ]);

        if($store){
            $tes = Carbon::now();
            $tes = $tes->toArray();
            $data['tanggal'] = $tes['day'].'-'.$tes['month'].'-'.$tes['year'];
            $data['penyewaan'] = Penyewaan::where('id_penyewaan', $store['id_penyewaan'])->first();
            return view('pages.penyewaan.struk', $data);

        }else{
            return redirect()->back()->with('error', 'Gagal menyelesaikan transaksi');
        }
    }

    public function selesai(Request $request)
    {
        $penyewaan = Penyewaan::find($request['id']);
        $update = $penyewaan->update(['status'=> 'Selesai']);

        $kolam_sewa = KolamSewa::find($penyewaan->id_kolam_sewa);
        $kolam_sewa->update([
            'status_kolam_sewa' => 0,
            'isi_kolam_sewa' => 0
        ]);

        if($update){
            return redirect()->back()->with('success', 'Berhasil menyelesaikan sewa');
        }else{
            return redirect()->back()->with('error', 'Gagal menyelesaikan sewa');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['penyewaan'] = Penyewaan::find($id);
        $data['kolam'] = KolamSewa::all();
        $data['stok'] = Kolam::select('isi_kolam')->get();
        $stok_ikan = 0;
        foreach($data['stok'] as $stok){
            $stok_ikan += $stok->isi_kolam;
        }
        $data['stok_ikan'] = $stok_ikan;
        return view('pages.penyewaan.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'tgl_sewa' => 'required',
            'stok' => 'required',
            'id_kolam_sewa' => 'required',
            'nama_penyewa' => 'required',
            'nohp' => 'required',
            'alamat' => 'required',
            'tangkapan' => '',
            'total_bayar' => '',
            'bayar' => '',
            'kembali' => '',
        ]);
        if($input['bayar'] != ''){
            if($input['bayar'] < $input['total_bayar']) return redirect()->back()->with('error', 'Uang dibayar kurang!');
        }
        $penyewaan = Penyewaan::find($id);
        $kolam = KolamSewa::find($penyewaan['id_kolam_sewa']);
        $kapasitas = $kolam->kapasitas_kolam_sewa - $penyewaan->tangkapan;
        if($input['tangkapan'] > $kapasitas) return redirect()->back()->with('error', 'Kapasitas kolam tidak cukup');

        $input['status'] = "Sewa";
        if($input['tangkapan'] == null){
            $update = $penyewaan->update([
                'tgl_sewa' => $input['tgl_sewa'],
                'nama_penyewa' => $input['nama_penyewa'],
                'nohp' => $input['nohp'],
                'alamat' => $input['alamat'],
            ]);
        }else{
            $update = $penyewaan->update([
                'tgl_sewa' => $input['tgl_sewa'],
                'nama_penyewa' => $input['nama_penyewa'],
                'nohp' => $input['nohp'],
                'alamat' => $input['alamat'],
                'tangkapan' => $penyewaan->tangkapan + $input['tangkapan'],
                'total_bayar' => $penyewaan->total_bayar + $input['total_bayar'],
                'bayar' => $penyewaan->bayar+ $input['bayar'],
                'kembali' => $penyewaan->kembali + $input['kembali'],
            ]);
        }
        $kolam->update([
            'isi_kolam_sewa' => $kolam->isi_kolam_sewa + $input['tangkapan']
        ]);

        if($update){
            if($input['tangkapan']){
                $kolam_stok = Kolam::all();
                $stok_ikan = 0;
                foreach($kolam_stok as $stok){
                    $stok_ikan += $stok->isi_kolam;
                }

                PenguranganIkan::create([
                    'asal' => $stok_ikan,
                    'pengurangan' => $input['tangkapan'],
                    'stok_sekarang' => $stok_ikan - $input['tangkapan'],
                ]);

                $sisa = $input['tangkapan'];
                $terpenuhi = false;
                foreach($kolam_stok as $item){
                    if($item->isi_kolam != 0){
                        $sisa -= $item->isi_kolam;
                        $isi = 0;
                        if($sisa <= 0){
                            $isi = $sisa * -1;
                            $terpenuhi = true;
                        }
                        $item->update(['isi_kolam' => $isi,'status_kolam' => $isi != 0 ? 1 : 0]);
                    }
                    if($terpenuhi) break;
                }

                $tes = Carbon::now();
                $tes = $tes->toArray();
                $data['tanggal'] = $tes['day'].'-'.$tes['month'].'-'.$tes['year'];
                $data['penyewaan'] = Penyewaan::where('id_penyewaan', $id)->first();
                return view('pages.penyewaan.struk', $data);
            }else{
                return redirect('/penyewaan')->with('success', 'Berhasil mengubah data penyewaan');
            }

        }else{
            return redirect()->back()->with('error', 'Gagal mengubah data penyewaan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penyewaan = Penyewaan::find($id);
        $delete = $penyewaan->delete();
        if($delete){
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Gagal menghapus data',
            ]);
        }
    }
}
