<?php

namespace App\Exports;

use App\Models\Barang;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithProperties;

class LaporanBarangExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('pages.barang.laporan',[
            'barang' => Barang::select('tgl_beli', 'nama_barang', 'jenis_barang', 'harga_barang', 'stok_barang')->get()
        ]);
    }

}

