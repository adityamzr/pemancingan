<?php

namespace App\Exports;

use App\Models\Barang;
use App\Models\Penyewaan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithProperties;

class LaporanPenyewaanExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('pages.penyewaan.laporan',[
            'barang' => Penyewaan::with('kolam')->get()
        ]);
    }

}

