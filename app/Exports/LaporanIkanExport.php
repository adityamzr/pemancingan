<?php

namespace App\Exports;

use App\Models\DetailIkan;
use App\Models\Ikan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class LaporanIkanExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('pages.ikan.laporan',[
            'detail' => DetailIkan::with('mitra')->get()
        ]);
    }   
}