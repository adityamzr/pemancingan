<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryBarang extends Model
{
    use HasFactory;

    protected $table = "t_history_barang";

    protected $primaryKey = 'id_history_barang';

    protected $fillable = [
        'id_history_barang',
        'id_barang',
        'id_user',
        'asal',
        'perubahan',
        'stok_sekarang',
        'keterangan',
        'type'
    ];

    public function user(){
        return $this->hasOne(User::class, 'id_user', 'id_user');
    }

    public function barang(){
        return $this->hasOne(Barang::class, 'id_user', 'id_user');
    }
}
