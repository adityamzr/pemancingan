<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailTransaksi extends Model
{
    use HasFactory;

    protected $table = 't_detail_transaksi';

    protected $primaryKey = 'id_detail_transaksi';

    protected $fillable = [
        'id_transaksi',
        'id_barang',
        'qty',
        'subtotal'
    ];

    public function barang(){
        return $this->hasOne(Barang::class, 'id_barang', 'id_barang');
    }

    public $timestamps = false;
}
