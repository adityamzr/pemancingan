<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KolamSewa extends Model
{
    use HasFactory;

    protected $table = "t_kolam_sewa";

    protected $primaryKey = 'id_kolam_sewa';

    protected $fillable = [
        'id_kolam_sewa',
        'nama_kolam_sewa',
        'kapasitas_kolam_sewa',
        'status_kolam_sewa',
        'isi_kolam_sewa'
    ];

    public $timestamps = false;
}
