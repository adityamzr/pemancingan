<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;

    protected $primariKey = 'id_transaksi';

    protected $table = 't_transaksi';

    protected $fillable = [
        'id_transaksi',
        'id_user',
        'nomor_transaksi',
        'total_bayar'
    ];

    public function user(){
        return $this->hasOne(User::class, 'id_user', 'id_user');
    }

    public function detail(){
        return $this->hasMany(DetailTransaksi::class, 'id_transaksi', 'id_transaksi');
    }

}
