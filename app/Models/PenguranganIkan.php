<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenguranganIkan extends Model
{
    use HasFactory;

    protected $table = "t_history_pengurangan_ikan";

    protected $primaryKey = 'id_pengurangan_ikan';

    protected $fillable = [
        'id_pengurangan_ikan',
        'asal',
        'pengurangan',
        'stok_sekarang',
    ];
}
