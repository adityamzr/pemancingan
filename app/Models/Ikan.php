<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ikan extends Model
{
    use HasFactory;

    protected $table = "t_ikan";

    protected $primaryKey = 'id_ikan';

    protected $fillable = [
        'id_ikan',
        'stok_ikan',
    ];

    public function detail(){
        return $this->hasMany(DetailIkan::class, 'id_ikan', 'id_ikan');
    }
}
