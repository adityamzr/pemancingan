<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailIkan extends Model
{
    use HasFactory;

    protected $table = "t_detail_ikan";

    protected $primaryKey = 'id_detail_ikan';

    protected $fillable = [
        'id_detail_ikan',
        'id_ikan',
        'id_mitra',
        'jenis_ikan',
        'jumlah_beli',
        'tgl_pembelian',
        'bukti_pembelian',
        'harga_beli',
        'jumlah_beli_kg'
    ];

    public function mitra(){
        return $this->hasOne(Mitra::class, 'id_mitra', 'id_mitra');
    }
}
