<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kolam extends Model
{
    use HasFactory;
    protected $table = "t_kolam";

    protected $primaryKey = 'id_kolam';

    protected $fillable = [
        'id_kolam',
        'id_ikan',
        'nama_kolam',
        'kapasitas_kolam',
        'status_kolam',
        'isi_kolam'
    ];

    public function ikan(){
        return $this->hasOne(Ikan::class, 'id_ikan', 'id_ikan');
    }

    public $timestamps = false;
}
