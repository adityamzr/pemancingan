<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penyewaan extends Model
{
    use HasFactory;

    protected $table = 't_penyewaan';

    protected $primaryKey = 'id_penyewaan';

    protected $fillable = [
        'id_penyewaan',
        'id_kolam_sewa',
        'nama_penyewa',
        'nohp',
        'alamat',
        'tangkapan',
        'tgl_sewa',
        'status',
        'bayar',
        'kembali',
        'total_bayar',

    ];

    public function kolam(){
        return $this->hasOne(KolamSewa::class, 'id_kolam_sewa', 'id_kolam_sewa');
    }
}
