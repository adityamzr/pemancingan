<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;

    protected $table = "t_barang";

    protected $primaryKey = 'id_barang';

    protected $fillable = [
        'id_barang',
        'nama_barang',
        'jenis_barang',
        'harga_barang',
        'deskripsi_barang',
        'tgl_beli',
        'bukti',
        'stok_barang'
    ];

    public $timestamps = false;
}
