<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mitra extends Model
{
    use HasFactory;

    protected $table = 't_mitra';

    protected $primaryKey = 'id_mitra';

    protected $fillable = [
        'id_mitra',
        'nama_mitra',
        'alamat',
        'kontak',
    ];

    public $timestamps = false;
}
