<!-- Page Header Start-->
<div class="page-main-header">
    <div class="main-header-right row m-0">
      <div class="main-header-left">
        <div class="logo-wrapper"><a href="index.html"><img class="img-fluid" style="max-width: 85%" src="{{ asset('assets') }}/images/logo/lauk.png" alt=""></a></div>
        <div class="dark-logo-wrapper"><a href="index.html"><img class="img-fluid" src="{{ asset('assets') }}/images/logo/dark-logo.png" alt=""></a></div>
        <div class="toggle-sidebar"><i class="status_toggle middle" data-feather="align-center" id="sidebar-toggle"></i></div>
      </div>
      <div class="nav-right col pull-right right-menu p-0">
        <ul class="nav-menus">
          <li class="onhover-dropdown">
            <div class="bookmark-box"><i data-feather="user"></i></div>
            <div class="bookmark-dropdown onhover-show-div">
              <ul class="m-t-5">
                <li class="add-to-bookmark">
                  <i class="bookmark-icon" data-feather="log-out"></i><a href="/logout">Log Out<span class="pull-right"></span></a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
      <div class="d-lg-none mobile-toggle pull-right w-auto"><i data-feather="more-horizontal"></i></div>
    </div>
  </div>
  <!-- Page Header Ends 