<!-- Page Sidebar Start-->
<header class="main-nav">
    <nav>
      <div class="main-navbar">
        <div id="mainnav">
          <ul class="nav-menu custom-scrollbar" style="padding-top: 15px">
            <li class="back-btn">
              <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
            </li>
            <li class="dropdown"><a class="nav-link menu-title link-nav active" href="/dashboard"><i data-feather="home"></i><span>Dashboard</span></a></li>
            <li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="shopping-cart"></i><span>Kantin</span></a>
              <ul class="nav-submenu menu-content">
                <li><a class="submenu-title link-nav" href="/transaksi">Transaksi</a>
                </li>
                <li><a class="submenu-title link-nav" href="/barang">Data Barang</a>
                </li>
                {{-- <li><a class="submenu-title link-nav" href="/laporanbarang">Laporan</a>
                </li> --}}
              </ul>
            </li>
            <li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="bookmark"></i><span>Sewa</span></a>
              <ul class="nav-submenu menu-content">
                <li><a class="submenu-title link-nav" href="/penyewaan">Penyewaan</a>
                </li>
                <li><a class="submenu-title link-nav" href="/sewa-kolam">Kolam Sewa</a>
                </li>
              </ul>
            </li>
            <li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="crop"></i><span>Ikan</span></a>
              <ul class="nav-submenu menu-content">
                <li><a class="submenu-title link-nav" href="/mitra">Mitra</a>
                </li>
                <li><a class="submenu-title link-nav" href="/ikan">Pembelian Ikan</a>
                </li>
                <li><a class="submenu-title link-nav" href="/pengurangan-ikan">History Pengurangan</a>
                </li>
              </ul>
            </li>
            <li class="dropdown"><a class="nav-link menu-title link-nav" href="/kolam"><i data-feather="inbox"></i><span>Kolam Stok</span></a></li>
            @if (auth()->user()->role == 0)
            <li class="dropdown"><a class="nav-link menu-title link-nav" href="/user"><i data-feather="users"></i><span>User</span></a></li>
            @endif
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <!-- Page Sidebar Ends-->