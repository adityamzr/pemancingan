@extends('layouts.body')

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Data Mitra</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/mitra">Mitra</a></li>
                <li class="breadcrumb-item">Form</li>
            </ol>
            </div>
        </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-header pb-0">
            </div>
            <div class="card-body pt-2">
                <form action="{{ url('/mitra', @$mitra->id_mitra) }}" method="POST" class="needs-validation" novalidate>
                    @csrf
                    @if (!empty($mitra))
                        @method('PATCH')
                    @endif
                    <div class="row g-3 mt-2">
                        <div class="col-6">
                            <label class="form-label" for="">Nama Mitra</label>
                            <input class="form-control" name="nama_mitra" value="{{ old('nama_mitra', @$mitra->nama_mitra) }}" type="text" placeholder="Masukan Nama Mitra" required>
                            <div class="invalid-feedback">
                                Nama Mitra tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="">Kontak</label>
                            <input class="form-control" name="kontak" value="{{ old('kontak', @$mitra->kontak) }}" type="number" placeholder="Masukan Kontak Mitra" required>
                            <div class="invalid-feedback">
                                Kontak tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 mt-2">
                        <div class="col-12">
                            <label class="form-label" for="">Alamat</label>
                            <textarea class="form-control" name="alamat" rows="3" placeholder="Masukan Alamat Mitra" required>{{ old('alamat', @$mitra->alamat) }}</textarea>
                            <div class="invalid-feedback">
                                Alamat tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 my-2">
                        <div class="col d-flex justify-content-end align-items-end">
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </div>
                </form>
            
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()
    </script>
@endpush