<table>
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Jenis Ikan</th>
        <th>Jumlah Beli</th>
        <th>Tempat Beli</th>
        <th>Alamat</th>
        <th>Kontak</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    @foreach($detail as $item)
        <tr>
            <td>{{ $item->tgl_pembelian }}</td>
            <td>{{ $item->jenis_ikan }}</td>
            <td>{{ $item->jumlah_beli }}</td>
            <td>{{ $item->mitra->nama_mitra }}</td>
            <td>{{ $item->mitra->alamat }}</td>
            <td>{{ $item->mitra->kontak }}</td>
            <td>{{ $item->harga_beli }}</td>
        </tr>
    @endforeach
    </tbody>
</table>