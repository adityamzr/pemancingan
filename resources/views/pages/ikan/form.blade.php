@extends('layouts.body')

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Data Ikan</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/ikan">Ikan</a></li>
                <li class="breadcrumb-item">Form</li>
            </ol>
            </div>
        </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-header pb-0">
            </div>
            <div class="card-body pt-2">
                <form action="{{ url('/ikan', @$detail_ikan->id_detail_ikan) }}" enctype="multipart/form-data" method="POST" class="needs-validation" novalidate>
                    @csrf
                    @if (!empty($detail_ikan))
                        @method('PATCH')
                    @endif
                    <div class="row g-3 mt-2">
                        <div class="col-12">
                            <label class="form-label" for="">Jenis Ikan</label>
                            <select class="form-select" name="jenis_ikan" id="" aria-label="select" required>
                                <option value="">--Pilih--</option>
                                <option value="Ikan Mas" {{ old('jenis_ikan', @$detail_ikan->jenis_ikan) == 'Ikan Mas' ? 'selected' : '' }}>Ikan Mas</option>
                            </select>
                            <div class="invalid-feedback">
                                Jenis Ikan tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 mt-2">
                        <div class="col-6">
                            <label class="form-label" for="">Jumlah Beli (Ekor)</label>
                            <input class="form-control" name="jumlah_beli" value="{{ old('jumlah_beli', @$detail_ikan->jumlah_beli) }}" type="number" required>
                            <div class="invalid-feedback">
                                Jumlah Beli tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="">Harga Beli (Kg)</label>
                            <input class="form-control" name="harga_beli" value="{{ old('harga_beli', @$detail_ikan->harga_beli) }}" type="number" required>
                            <div class="invalid-feedback">
                                Harga Beli tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 mt-2">
                        <div class="col-6">
                            <label class="form-label" for="">Tempat Beli</label>
                            <select class="form-select" name="id_mitra" id="" aria-label="select" required>
                                <option value="">--Pilih--</option>
                                @foreach ($mitra as $item)
                                    <option value="{{ $item->id_mitra }}" {{ old('id_mitra', @$detail_ikan->id_mitra) == $item->id_mitra ? 'selected' : '' }}>{{ $item->nama_mitra }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Tempat Beli tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="">Tanggal Beli</label>
                            <input class="form-control" name="tgl_pembelian" value="{{ old('tgl_pembelian', @$detail_ikan->tgl_pembelian) }}" type="date" required>
                            <div class="invalid-feedback">
                                Tanggal Beli tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 mt-2">
                        <div class="col-6">
                            <label class="form-label" for="">Jumlah Beli (Kg)</label>
                            <input class="form-control" name="jumlah_beli_kg" value="{{ old('jumlah_beli_kg', @$detail_ikan->jumlah_beli_kg) }}" type="number" required>
                            <div class="invalid-feedback">
                                Jumlah Beli tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="">Bukti Pembelian</label>
                            <input type="file" class="form-control" name="bukti_pembelian" {{ old('bukti_pembelian', @$detail_ikan->bukti_pembelian) != null ? '' : 'required' }}>
                            <div class="invalid-feedback">
                                Bukti Pembelian tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 my-2">
                        <div class="col d-flex justify-content-end align-items-end">
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </div>
                </form>

            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()
    </script>
@endpush
