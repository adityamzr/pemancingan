@extends('layouts.body')

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Data User</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/user">User</a></li>
                <li class="breadcrumb-item">Form</li>
            </ol>
            </div>
        </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-header pb-0">
            </div>
            <div class="card-body pt-2">
                <form action="{{ url('/user', @$user->id_user) }}" method="POST" class="needs-validation" novalidate>
                    @csrf
                    @if (!empty($user))
                        @method('PATCH')
                    @endif
                    <div class="row g-3">
                        <div class="col-12">
                            <label class="form-label" for="">Nama User</label>
                            <input class="form-control" name="nama_user" value="{{ old('nama_user', @$user->nama_user) }}" type="text" required>
                            <div class="invalid-feedback">
                                Nama User tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 mt-2">
                        <div class="col-6">
                            <label class="form-label" for="">Username</label>
                            <input class="form-control" name="username" value="{{ old('username', @$user->username) }}" type="text" required>
                            <div class="invalid-feedback">
                                Username tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="">Password</label>
                            <input class="form-control" name="password" value="{{ old('') }}" type="password" required>
                        </div>
                    </div>
                    <div class="row g-3 mt-2">
                        <div class="col-6">
                            <label class="form-label" for="">Jabatan</label>
                            <input class="form-control" name="jabatan" value="{{ old('jabatan', @$user->jabatan) }}" type="text" required>
                            <div class="invalid-feedback">
                                Jabatan tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="">Role</label>
                            <select class="form-select" id="" name="role" required>
                                <option value="">--Pilih--</option>
                                <option value="0" {{ old('role', @$user->role) == '0' ? 'selected' : '' }}>Admin</option>
                                <option value="1" {{ old('role', @$user->role) == '1' ? 'selected' : '' }}>Petugas</option>
                            </select>
                            <div class="invalid-feedback">
                                Role tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 my-2">
                        <div class="col d-flex justify-content-end align-items-end">
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </div>
                </form>
            
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()
    </script>
@endpush