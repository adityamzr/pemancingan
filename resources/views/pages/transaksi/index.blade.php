@extends('layouts.body')

@section('content')
<div class="page-body">
    {{-- <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Transaksi</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item">Transaksi</li>
            </ol>
            </div>
        </div>
        </div>
    </div> --}}
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body pt-2">
                    <div class="dt-ext mt-3">
                        <table class="display f12 tabel" id="tabel">
                        <thead>
                            <tr>
                            <th class="">Nama Barang</th>
                            <th class="text-center">Jenis</th>
                            <th class="text-center">Harga</th>
                            <th class="text-center">Deskripsi</th>
                            <th class="text-center">Stok</th>
                            <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-auto">
                            <i data-feather="shopping-bag"></i>
                        </div>
                        <div class="col-auto px-0">
                            <h5 class="m-0">Keranjang</h5>
                        </div>
                    </div>
                  <hr class="my-3">
                <div class="row">
                  <div class="order-history table-responsive wishlist">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Nama Barang</th>
                          <th>Harga</th>
                          <th>Quantity</th>
                          <th>Aksi</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody id="cart">
                        @include('pages.transaksi.cart_items')
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <!-- Container-fluid Ends-->
    </div>
</div>
<div class="toast-container">
    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11" id="toast">
        
    </div>
</div>
<div class="modal fade" id="modalPembayaran" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Pembayaran</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="/transaksi" method="POST">
            @csrf
            <div class="modal-body">
            <div class="row mb-2">
                <div class="col-4">
                    <h6>Total Bayar</h6>
                </div>
                <div class="col-4">
                    : <span id="pembayaranTotal"></span>
                    <input type="hidden" id="total_bayar" name="total_bayar">
                </div>
            </div>
            <div class="row">
                <div class="col-4 d-flex align-items-center">
                    <span>Dibayar</span>
                </div>
                <div class="col-4">
                    <input type="number" id="pembayaranDibayar" name="bayar" class="form-control form-control-sm" required>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-4">
                    <span>Kembali</span>
                </div>
                <div class="col-4">
                    : <span id="pembayaranKembali"></span>
                    <input type="hidden" id="kembali" name="kembali">
                </div>
            </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-struk">Cetak Struk</button>
            </div>
        </form>
      </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $( document ).ready(function() {

        var table, id_barang, nama_barang;
        table = $('#tabel').DataTable({
            pageLength: 5,
            lengthMenu: [[5], [5]],
            deferRender: true,
            serverSide: true,
            processing: true,
            stateSave: true,
            ajax: {
                url: '{!! route('transaksi.get') !!}',
                type: 'POST',
                data: function (e) {
                    e._token = '{{ csrf_token() }}';
                }
            },
            drawCallback: function (){
                // feather.replace();
            }
        });

    });

    $('body').on('click', '.btn-pilih', function(event){
        id_barang = $(this).data('id-barang')
        nama_barang = $(this).data('nama-barang')
        // console.log(id_barang)

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{!! route('storeItem.post') !!}',
            type: 'POST',
            data: {
                "id_barang": id_barang,
                "_token": "{{ csrf_token() }}"
            }, success: function(data){
                event.preventDefault()
                console.log(data)
                $('#toast').html(`
                <div class="toast text-white bg-primary" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <strong class="me-auto">Notifikasi</strong>
                        <small class="text-muted">just now</small>
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="toast-body">
                        Berhasil menambah `+nama_barang+` ke keranjang.
                    </div>
                </div>
                `)
                $('.toast').toast('show')
                $('#cart').html(data.data.cart_items)
                $('.touchspin').TouchSpin({buttondown_class:"btn btn-primary btn-square btn-down",buttonup_class:"btn btn-primary btn-square btn-up",buttondown_txt:'<i class="fa fa-minus"></i>',buttonup_txt:'<i class="fa fa-plus"></i>'})
                feather.replace()
            }
        })
    })

    $('body').on('click', '.btn-remove', function(event){
        id_barang = $(this).data('id-barang')
        nama_barang = $(this).data('nama-barang')
        // console.log(id_barang)
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'transaksi/removeItem/'+id_barang,
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}"
            }, success: function(data){
                event.preventDefault()
                console.log(data)
                $('#toast').html(`
                <div class="toast text-white bg-danger" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <strong class="me-auto">Notifikasi</strong>
                        <small class="text-muted">just now</small>
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="toast-body">
                        Berhasil menghapus `+nama_barang+` dari keranjang.
                    </div>
                </div>
                `)
                $('.toast').toast('show')
                $('#cart').html(data.data.cart_items)
                $('.touchspin').TouchSpin({buttondown_class:"btn btn-primary btn-square",buttonup_class:"btn btn-primary btn-square",buttondown_txt:'<i class="fa fa-minus"></i>',buttonup_txt:'<i class="fa fa-plus"></i>'})
                feather.replace()
            }
        })
    })

    $('body').on('click', '.btn-bayar', function(event){
        var total, bayar, temp, kembali, x, id_barang, qty;
        $('#pembayaranKembali').text('');
        $('#kembali').val(''); 
        $('#pembayaranDibayar').val('');
        id_barang  = $(this).data('id-barang')
        qty = $(this).data('qty-barang')
        total = $(this).data('total')
        $('#pembayaranTotal').text(total)
        $('.btn-struk').attr('disabled', true);

        $('#pembayaranDibayar').keyup(function() {
            bayar = $('#pembayaranDibayar').val();
            temp = bayar-total;
            x = +bayar;
            console.log(x);
            
            var number_string = temp.toString(),
                sisa = number_string.length % 3,
                rupiah = number_string.substr(0, sisa),
                ribuan = number_string.substr(sisa).match(/\d{3}/g);

                if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                    var rp = 'Rp' + rupiah;
                }
                $('#pembayaranKembali').text(rupiah);
                $('#kembali').val(temp);     
                $('#total_bayar').val(total);     
            if(x >= total){
                $('.btn-struk').attr('disabled', false);
            }else{
                $('.btn-struk').attr('disabled', true);
            }
        })

    })
    
    $('body').on('click', '.bootstrap-touchspin-up', function(){
        id_barang = $('#id-barang').val()
        console.log(id_barang)
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'transaksi/qtyUp/'+id_barang,
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}"
            }, success: function(data){
                event.preventDefault()
                console.log(data)
                $('#cart').html(data.data.cart_items)
                $('.touchspin').TouchSpin({buttondown_class:"btn btn-primary btn-square",buttonup_class:"btn btn-primary btn-square",buttondown_txt:'<i class="fa fa-minus"></i>',buttonup_txt:'<i class="fa fa-plus"></i>'})
                feather.replace()
            }
        })
    })

    $('body').on('click', '.bootstrap-touchspin-down', function(){
        id_barang = $('#id-barang').val()
        console.log(id_barang)
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'transaksi/qtyDown/'+id_barang,
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}"
            }, success: function(data){
                event.preventDefault()
                console.log(data)
                $('#cart').html(data.data.cart_items)
                $('.touchspin').TouchSpin({buttondown_class:"btn btn-primary btn-square",buttonup_class:"btn btn-primary btn-square",buttondown_txt:'<i class="fa fa-minus"></i>',buttonup_txt:'<i class="fa fa-plus"></i>'})
                feather.replace()
            }
        })
    })

</script>
@endpush

