@php
    function titik($angka){
        $hasil_rupiah = "" . number_format($angka,0,'','.');

        return $hasil_rupiah;
    }
@endphp
@if (!empty(session()->get('cart_items')))
    @foreach (session()->get('cart_items') as $item)
    <tr>
        <td>
            <h6>{{ $item['barang']->nama_barang }}</h6>
        </td>
        <td>{{ titik($item['barang']->harga_barang) }}</td>
        <td>
            <input type="hidden" id="id-barang" value="{{ $item['barang']->id_barang }}">
            <fieldset class="qty-box">
            <div class="input-group">
                <input class="touchspin text-center" type="text" value="{{ $item['qty'] }}">
            </div>
            </fieldset>
        </td>
        <td><i data-feather="x-circle" class="btn-remove" data-id-barang="{{ $item['barang']->id_barang }}" data-nama-barang="{{ $item['barang']->nama_barang }}"></i></td>
    <td>{{ titik($item['subtotal']) }}</td>
    </tr>
    @endforeach
    <tr>
        <td colspan="3">     
        </td>
        <td class="total-amount">
        <h6 class="m-0 text-end"><span class="f-w-600">Total Bayar :</span></h6>
        </td>
        <td><span>{{ titik(session()->get('cart_total')) }}</span></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td class="w-100"><button type="button" class="btn btn-success cart-btn-transform btn-bayar" data-total="{{ session()->get('cart_total') }}" data-bs-toggle="modal" data-bs-target="#modalPembayaran">Bayar</button></td>
    </tr>
@else
    <tr>
        <td colspan="5">Belum ada barang yang masuk keranjang</td>
    </tr>
@endif
