<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('assets') }}/images/logo/favicon.png" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('assets') }}/custom.css"> --}}
    <title>Pemancingan | Print Struk</title>
</head>
<style>
    body{
        background-color: white;
    }
    .content-struk{
        width: 350px;
        min-height: 140px; 
        margin: auto;
        padding: 10px;
    }
    .hr-line{
        border: none; 
        border-top: 2px dashed black; 
        background-color: transparent;
    }
    .logo{
        width: 80px; height: 80px;
    }
    .f8{
        font-size: 8pt;
    }
    .tabel-atas td{
        width: 95px;
        font-size: 10pt;
    }
    .tabel-tengah th{
        width: 220px;
    }
    .tabel-tengah td{
        padding: 5px 0px;
    }
    .tabel-bawah td{
        width: 163px;
    }
</style>
<body>
    <div class="container" style="min-height: 250px;">
        <div class="content-struk mt-4">
            <div class="text-center mt-3" style="margin: auto;">
                <div class="col">
                    <img src="{{ asset('assets') }}/images/logo/lauk.png" class="logo" width="90px">
                </div>
                <div class="col">
                    <h6>Pemancingan</h6>
                </div>
                <div class="col mx-auto" style="max-width: 250px;">
                    <p class="f8 roboto-light">Jl. Blablablablaa.</p>
                </div>
                <hr class="hr-line">
            </div>
            <div class="col-12">
                <table class="tabel-atas">
                    <tr>
                        <td>No. Transaksi</td>
                        <td>: {{ $transaksi->nomor_transaksi }}</td>
                    </tr>
                    <tr>
                        <td>Kasir</td>
                        <td style="width: 200px;">: {{ $transaksi->user->nama_user }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>: </td>
                    </tr>
                </table>
                <hr class="hr-line">
                @php
                    function titik($angka){

                    $hasil_rupiah = "" . number_format($angka,0,'','.');
                    return $hasil_rupiah;

                    }
                @endphp
                @foreach ($detail as $item)
                <div>
                    <span class="f12 roboto-medium">{{ $item->barang->nama_barang }}</span>
                    <table class="tabel-tengah">
                        <tr>
                            <td style="width: 60px;">{{ titik($item->barang->harga_barang) }}</td>
                            <td style="width: 14px;">x</td>
                            <td>{{ $item->qty }}</td>
                            <td class="text-end" style="width: 250px;">{{ titik($item->subtotal) }}</td>
                        </tr>
                    </table>
                </div>
                @endforeach
                <hr class="hr-line">
                <table class="tabel-bawah">
                    <tr>
                        <td class="f16 roboto-bold">Total</td>
                        <td class="f16 roboto-bold text-end">{{ titik($transaksi->total_bayar) }}</td>
                    </tr>
                </table>
                <hr class="hr-line">
                <table class="tabel-bawah">
                    <tr>
                        <td>Bayar</td>
                        <td class="text-end">{{ titik($bayar) }}</td>
                    </tr>
                    <tr>
                        <td>Kembali</td>
                        <td class="text-end">{{ titik($kembali) }}</td>
                    </tr>
                </table>
            </div>
            <div>
                <h5 class="my-5 text-center">--Terima Kasih--</h5>
            </div>
        </div>
    </div>
    <script>
        window.print();
        window.onafterprint = function(event) {
            window.location.href = "http://127.0.0.1:8000/transaksi";
        }
    </script>
</body>
</html>