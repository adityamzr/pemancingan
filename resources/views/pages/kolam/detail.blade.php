@extends('layouts.body')

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Data Kolam</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/kolam">Kolam</a></li>
                <li class="breadcrumb-item">Detail</li>
            </ol>
            </div>
        </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-6">
            <div class="card">
              <div class="card-header pb-0">
              </div>
              <div class="card-body pt-2">
                  <div class="row mb-2">
                    <div class="col-5"><h6>Nama Kolam</h6></div>
                    <div class="col-1">:</div>
                    <div class="col-6"><span>{{ $kolam->nama_kolam }}</span></div>
                  </div>
                  <div class="row mb-2">
                    <div class="col-5"><h6>Kapasitas Kolam</h6></div>
                    <div class="col-1">:</div>
                    <div class="col-6"><span>{{ $kolam->kapasitas_kolam }}</span></div>
                  </div>
                  <div class="row mb-2">
                    <div class="col-5"><h6>Status</h6></div>
                    <div class="col-1">:</div>
                    <div class="col-6"><span>
                      @if ($kolam->status_kolam == 0)
                          Kosong
                      @elseif($kolam->status_kolam == $kolam->kapasitas_kolam)
                        Penuh
                      @else
                          Terisi
                      @endif
                    </span></div>
                  </div>
                  <div class="row mb-2">
                    <div class="col-5"><h6>Isi Kolam Saat Ini</h6></div>
                    <div class="col-1">:</div>
                    <div class="col-6"><span>
                        @if ($kolam->isi_kolam == 0)
                            -
                        @else
                            {{ $kolam->isi_kolam }}
                        @endif
                    </span></div>
                  </div>
              </div>
              <button class="btn btn-primary btn-modal">Tambah Isi Kolam</button>
            </div>
          </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Masukan Ikan ke Kolam</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="" method="POST" id="myForm" class="needs-validation" novalidate>
            @csrf
            <input type="hidden" id="id_kolam" value="{{ $kolam->id_kolam }}">
            <div class="modal-body">
                <div class="col-12 my-3">
                    <div class="row my-2">
                        <div class="col-5">
                            Kapasitas Kosong Saat Ini
                        </div>
                        <div class="col-5">
                            : <span id="kapasitas">{{ $kolam->kapasitas_kolam - $kolam->isi_kolam }}</span>
                        </div>
                    </div>
                    <div class="row my-2">
                        <div class="col-5">
                            Isi Kolam Saat Ini
                        </div>
                        <div class="col-5">
                            : <span id="isi">{{ $kolam->isi_kolam }}</span>
                        </div>
                    </div>
                    <div class="row my-2">
                        <div class="col-5">
                            <p>Stok Ikan yang belum masuk</p>
                        </div>
                        <div class="col-5">
                            : <span id="stok">{{ $ikan->stok_ikan }}</span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-5">
                            Masukan
                        </div>
                        <div class="col-6">
                            <fieldset class="qty-box">
                                <div class="input-group bootstrap-touchspin">
                                    <input class="touchspin text-center form-control" type="number" id="data-ikan" name="data-ikan" required>
                                    <div class="invalid-feedback" id="feedback">

                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-3 mt-4" id="div-btn-kosongkan">
                  @if ($kolam->isi_kolam != 0)
                    <button type="submit" id="btn-kosongkan" class="btn btn-danger btn-sm w-100">Kosongkan Kolam</button>
                  @endif
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <button type="submit" id="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()

        $('body').on('click', '.btn-modal', function(event){
          $('.modal').modal('show');
          const id_kolam = $('#id_kolam').val()
          console.log(id_kolam)

        $('#submit').on('click', function(e){
            e.preventDefault()
            const qty = parseInt($('#stok').text());
            const kapasitas = parseInt($('#kapasitas').text());
            const qtyIkan = $('#data-ikan').val();
            const input = $('#myForm').find(':input');
            const valid = !Array.from(input.map(function(index, child){
                return child.checkValidity();
            })).includes(false);
            if(valid == false){
                e.preventDefault()
                $('#feedback').html(`
                <span>Tidak boleh kosong!</span>
                `)
                $('form').addClass('was-validated')
            }else if(valid == true){
                if(qtyIkan <= 0){
                    e.preventDefault()
                    Swal.fire({
                        position: 'top',
                        icon: 'error',
                        title: 'Error!',
                        text: 'Data tidak boleh kurang dari 1!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }else if(qtyIkan > qty){
                    e.preventDefault()
                    Swal.fire({
                        position: 'top',
                        icon: 'error',
                        title: 'Error!',
                        text: 'Data tidak boleh lebih dari stok!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }else if(qtyIkan <= qty && qtyIkan > kapasitas){
                    e.preventDefault()
                    Swal.fire({
                        position: 'top',
                        icon: 'error',
                        title: 'Error!',
                        text: 'Data tidak boleh lebih dari kapasitas saat ini!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }else{
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{!! route('storeIkan.post') !!}',
                        type: 'POST',
                        data: {
                            "id": id_kolam,
                            "stok_ikan": qtyIkan,
                            "_token": "{{ csrf_token() }}"
                        }, success: function(data){
                            Swal.fire({
                                position: 'top-center',
                                icon: 'success',
                                title: 'Success!',
                                text: 'Berhasil mengisi kolam!',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            setTimeout(function() {
                              window.location.href = "/kolam/"+id_kolam;
                            }, 1000);
                        }

                    })
                }
            }
        })

        $('#btn-kosongkan').on('click', function(e){
            e.preventDefault()
            Swal.fire({
                title: 'Peringatan!',
                text: 'Kosongkan '+nama_kolam+'?',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                confirmButtonText: 'Kosongkan',
            }).then((result) => {
                if (result.isConfirmed) {
                console.log(id_kolam);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "kolam/resetKolam/"+id_kolam,
                    type: 'POST',
                    data: {
                    "id": id_kolam,
                    "_token": "{{ csrf_token() }}"
                    },
                    success: function(data){
                        console.log(data)
                        Swal.fire({
                            position: 'top-center',
                            icon: 'success',
                            title: 'Success!',
                            text: 'Berhasil mengosongkan kolam!',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function(){
                            location.reload()
                        })
                    }
                });
                } else if (result.isDenied) {
                Swal.fire('Kolam gagal dikosongkan!', '', 'info')
                }
            })
        })
    })
    </script>
@endpush
