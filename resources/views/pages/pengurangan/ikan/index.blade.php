@extends('layouts.body')

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Data Pengurangan Ikan</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item">Pengurangan Ikan</li>
            </ol>
            </div>
        </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-header pb-0">
            </div>
            <hr>
            <div class="card-body pt-2">
                <div class="dt-ext table-responsive mt-3">
                    <table class="display f12 tabel" id="tabel">
                    <thead>
                        <tr>
                        <th class="col-1">No</th>
                        <th class="col-2 text-center">Tanggal</th>
                        <th class="col-2 text-center">Jam</th>
                        <th class="col-3 text-center">Jumlah Asal</th>
                        <th class="col-2 text-center">Pengurangan</th>
                        <th class="col-2 text-center">Stok Sekarang</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection

@push('script')
<script>
    $( document ).ready(function() {

        var table;
        table = $('#tabel').DataTable({
            deferRender: true,
            serverSide: true,
            processing: true,
            stateSave: true,
            ajax: {
                url: '{!! route('pengurangan-ikan.get') !!}',
                type: 'POST',
                data: function (e) {
                    e._token = '{{ csrf_token() }}';
                }
            },
            drawCallback: function (){
                // feather.replace();
            }
        });
    });
</script>
@endpush