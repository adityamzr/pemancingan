@extends('layouts.body')

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Data Kolam Sewa</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/sewa-kolam">Kolam Sewa</a></li>
                <li class="breadcrumb-item">Form</li>
            </ol>
            </div>
        </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-header pb-0">
            </div>
            <div class="card-body pt-2">
                <form action="{{ url('/sewa-kolam', @$kolam_sewa->id_kolam_sewa) }}" method="POST" class="needs-validation" novalidate>
                    @csrf
                    @if (!empty($kolam_sewa))
                        @method('PATCH')
                    @endif
                    <div class="row g-3 mt-2">
                        <div class="col-6">
                            <label class="form-label" for="">Nama Kolam</label>
                            <input class="form-control" name="nama_kolam_sewa" value="{{ old('nama_kolam_sewa', @$kolam_sewa->nama_kolam_sewa) }}" type="text" required>
                            <div class="invalid-feedback">
                                Nama Kolam tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-4">
                            <label class="form-label" for="">Kapasitas Kolam</label>
                            <input class="form-control" name="kapasitas_kolam_sewa" value="{{ old('kapasitas_kolam_sewa', @$kolam_sewa->kapasitas_kolam_sewa) }}" type="number" required>
                            <div class="invalid-feedback">
                                Kapistas Kolam tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-2 d-flex align-items-center">
                            <span class="pt-4">- Ikan</span>
                        </div>
                    </div>
                    <div class="row g-3 my-2">
                        <div class="col d-flex justify-content-end align-items-end">
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </div>
                </form>
            
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()
    </script>
@endpush