@extends('layouts.body')

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Data Barang</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/barang">Barang</a></li>
                <li class="breadcrumb-item">Form</li>
            </ol>
            </div>
        </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-header pb-0">
            </div>
            <div class="card-body pt-2">
                <form action="{{ url('/barang', @$barang->id_barang) }}" enctype="multipart/form-data" method="POST" class="needs-validation" novalidate>
                    @csrf
                    @if (!empty($barang))
                        @method('PATCH')
                    @endif
                    <input type="hidden" id="id_barang" value="{{ @$barang->id_barang }}">
                    <div class="row g-3">
                        <div class="col-6">
                            <label class="form-label" for="">Nama Barang</label>
                            <input class="form-control" name="nama_barang" value="{{ old('nama_barang', @$barang->nama_barang) }}" type="text" required>
                            <div class="invalid-feedback">
                                Nama Barang tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="">Jenis Barang</label>
                            <select class="form-select" id="" name="jenis_barang" required>
                                <option value="">--Pilih--</option>
                                <option value="Makanan" {{ old('jenis_barang', @$barang->jenis_barang) == 'Makanan' ? 'selected' : '' }}>Makanan</option>
                                <option value="Minuman" {{ old('jenis_barang', @$barang->jenis_barang) == 'Minuman' ? 'selected' : '' }}>Minuman</option>
                                <option value="Umpan" {{ old('jenis_barang', @$barang->jenis_barang) == 'Umpan' ? 'selected' : '' }}>Umpan</option>
                            </select>
                            <div class="invalid-feedback">
                                Jenis Barang tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 mt-2">
                        <div class="col-6">
                            <label class="form-label" for="">Stok Barang</label>
                            <div class="input-group">
                                <input class="form-control" name="stok_barang" id="stok" value="{{ old('stok_barang', @$barang->stok_barang) }}" type="number" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">/ pcs</span>
                                </div>
                            </div>

                            <div class="invalid-feedback">
                                Stok Barang tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="">Harga Jual</label>
                            <div class="input-group">
                                <input class="form-control" name="harga_barang" value="{{ old('harga_barang', @$barang->harga_barang) }}" type="number" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">/ pcs</span>
                                </div>
                            </div>

                            <div class="invalid-feedback">
                                Harga Barang tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 mt-2">
                        <div class="col-6">
                            <label class="form-label" for="">Tanggal Pembelian</label>
                            <input class="form-control" name="tgl_beli" value="{{ old('tgl_beli', @$barang->tgl_beli) }}" type="date" required>
                            <div class="invalid-feedback">
                                Tanggal tidak boleh kosong.
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label" for="">Expired Barang</label>
                            <input class="form-control" name="deskripsi_barang" value="{{ old('deskripsi_barang', @$barang->deskripsi_barang) }}" type="date" required>
                            <div class="invalid-feedback">
                                Tanggal tidak boleh kosong.
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 mt-2">
                        <div class="col-12">
                            <label class="form-label" for="">Bukti Pembelian</label>
                            <input class="form-control" name="bukti" value="{{ old('bukti', @$barang->bukti) }}" type="file">
                        </div>
                    </div>
                    @if (!empty($barang))
                    <div class="row g-3 mt-2">
                        <div class="col-12">
                            <div id="ket">
                                <label class="form-label" for="">Keterangan Diubah</label>
                                <input class="form-control" name="keterangan" type="text">
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row g-3 my-2">
                        <div class="col d-flex justify-content-end">
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
        </div>
        @if (!empty($barang))
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>History Barang</h5>
                    </div>
                    <div class="card-body pt-2">
                        <div class="dt-ext table-responsive mt-3">
                            <table class="display f12 tabel" id="tabel">
                            <thead>
                                <tr>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Jam</th>
                                <th class="text-center">Oleh</th>
                                <th class="text-center">Jumlah Asal</th>
                                <th class="text-center">Ditambah</th>
                                <th class="text-center">Dikurang</th>
                                <th class="text-center">Stok Sekarang</th>
                                <th class="text-center">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()

        $( document ).ready(function() {
            $('#ket').hide()
            let stok = $('#stok').val()
            $('#stok').keyup(function(){
                let newStok = $('#stok').val()
                if(stok === newStok){
                    return
                }else{
                    $('#ket').show()
                    $('#ket').html(`
                        <label class="form-label" for="">Keterangan Diubah</label>
                        <input class="form-control" name="keterangan" type="text" required>
                        <div class="invalid-feedback">
                            Keterangan tidak boleh kosong.
                        </div>
                    `)
                }
            })

            var table;
            var id = $('#id_barang').val();
            table = $('#tabel').DataTable({
                deferRender: true,
                serverSide: true,
                processing: true,
                stateSave: true,
                ajax: {
                    url: `/barang/${id}/edit/getData`,
                    type: 'POST',
                    data: function (e) {
                        e._token = '{{ csrf_token() }}';
                    }
                },
                drawCallback: function (){
                    // feather.replace();
                }
            });
        });
    </script>
@endpush
