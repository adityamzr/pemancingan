<table>
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Nama Barang</th>
        <th>Jenis Barang</th>
        <th>Harga Barang</th>
        <th>Stok Barang</th>
    </tr>
    </thead>
    <tbody>
    @foreach($barang as $item)
        <tr>
            <td>{{ $item->tgl_beli }}</td>
            <td>{{ $item->nama_barang }}</td>
            <td>{{ $item->jenis_barang }}</td>
            <td>{{ $item->harga_barang }}</td>
            <td>{{ $item->stok_barang }}</td>
        </tr>
    @endforeach
    </tbody>
</table>