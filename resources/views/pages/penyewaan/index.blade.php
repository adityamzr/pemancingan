@extends('layouts.body')

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Daftar Penyewaan</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item">Penyewaan</li>
            </ol>
            </div>
        </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col pull-right">
                        <a href="/penyewaan/create" class="btn btn-primary" type="button"><i class="fa-solid fa-plus"></i> Tambah</a>
                    </div>
                    <div class="col text-end pull-right">
                        <a href="/penyewaan/export_excel" class="btn btn-success" type="button"><i class="fa-solid fa-download"></i> Export Excel</a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="card-body pt-2">
                <div class="dt-ext table-responsive mt-3">
                    <table class="display f12 tabel" id="tabel">
                    <thead>
                        <tr>
                        <th class="col-1">No</th>
                        <th class="col-auto">Tanggal</th>
                        <th class="col-auto">Kolam</th>
                        <th class="col-auto">Nama</th>
                        <th class="col-auto text-center">Nomor Handphone</th>
                        <th class="col-auto text-center">Alamat</th>
                        <th class="col-auto text-center">Tangkapan</th>
                        <th class="col-auto text-center">Bayar</th>
                        <th class="col-auto text-center">Status</th>
                        <th class="col-auto text-center">Aksi</th>
                        {{-- @if ($penyewaan['status'] != 'Selesai')
                        @endif --}}
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection

@push('script')
<script>
    $( document ).ready(function() {
        var table;
        table = $('#tabel').DataTable({
            deferRender: true,
            serverSide: true,
            processing: true,
            stateSave: true,
            ajax: {
                url: '{!! route('penyewaan.get') !!}',
                type: 'POST',
                data: function (e) {
                    e._token = '{{ csrf_token() }}';
                }
            },
            drawCallback: function (){
                // feather.replace();
            }
        });
    });

    $('body').on('click', '.btn-selesai', function(event){
      event.preventDefault();
      var id = $(this).data('id');
      var nama = $(this).data('nama');

      Swal.fire({
        title: 'Peringatan!',
        text: 'Selesaikan penyewaan atas nama '+nama+'?',
        showCancelButton: true,
        confirmButtonColor: '#168eea',
        confirmButtonText: 'Selesai',
    }).then((result) => {
        if (result.isConfirmed) {
          console.log(id);
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "penyewaann",
            type: 'POST',
            data: {
              "id": id,
              "_token": "{{ csrf_token() }}"
            },
            success: function(data){
              Swal.fire({
              position: 'top-center',
              icon: 'success',
              title: 'Success!',
              text: 'Berhasil mengupdate data!',
              showConfirmButton: false,
              timer: 1500
              })
              $('#tabel').DataTable().ajax.reload()
            }
        });
        } else if (result.isDenied) {
          Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
  })

    $('body').on('click', '.btn-hapus', function(event){
      event.preventDefault();
      var id = $(this).data('hapus-id');
      var nama = $(this).data('hapus-nama');

      Swal.fire({
        title: 'Peringatan!',
        text: 'Hapus penyewaan kolam atas nama '+nama+'?',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Hapus',
    }).then((result) => {
        if (result.isConfirmed) {
          console.log(id);
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "penyewaan/"+id,
            type: 'DELETE',
            data: {
              "id": id,
              "_token": "{{ csrf_token() }}"
            },
            success: function(data){
              Swal.fire({
              position: 'top-center',
              icon: 'success',
              title: 'Success!',
              text: 'Berhasil menghapus data!',
              showConfirmButton: false,
              timer: 1500
              })
              $('#tabel').DataTable().ajax.reload()
            }
        });
        } else if (result.isDenied) {
          Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
  })
</script>
@endpush
