<table>
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Kolam</th>
        <th>Nama</th>
        <th>Nomor</th>
        <th>Alamat</th>
        <th>Tangkapan</th>
        <th>Bayar</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($barang as $item)
        <tr>
            <td>{{ $item->tgl_sewa }}</td>
            <td>{{ $item->kolam->nama_kolam_sewa }}</td>
            <td>{{ $item->nama_penyewa }}</td>
            <td>{{ $item->nohp }}</td>
            <td>{{ $item->alamat }}</td>
            <td>{{ $item->tangkapan }}</td>
            <td>{{ $item->total_bayar }}</td>
            <td>{{ $item->status }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
