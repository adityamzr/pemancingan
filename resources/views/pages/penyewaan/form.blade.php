@extends('layouts.body')

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
        <div class="row">
            <div class="col-sm-6">
            <h3>Data Penyewaan</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/penyewaan">Penyewaan</a></li>
                <li class="breadcrumb-item">Form</li>
            </ol>
            </div>
        </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row starter-main">
        <div class="col-sm-12">
            <form action="{{ url('/penyewaan', @$penyewaan->id_penyewaan) }}" id="myForm" method="POST" class="needs-validation" novalidate>
            <div class="card">
                <div class="card-header pb-0">
                    <h6 class="mb-0">Data Penyewa</h6>
                </div>
                <div class="card-body pt-1">
                        @csrf
                        @if (!empty($penyewaan))
                            @method('PATCH')
                        @endif
                        <div class="row g-3 mt-2">
                            <div class="col-6">
                                <label class="form-label" for="">Tanggal Sewa</label>
                                <input class="form-control" name="tgl_sewa" value="{{ old('tgl_sewa', @$penyewaan->tgl_sewa) }}" type="date" required>
                                <div class="invalid-feedback">
                                    Tanggal Sewa tidak boleh kosong.
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="form-label" for="">Kolam</label>
                                <select class="form-select" name="id_kolam_sewa" id="id_kolam_sewa" {{ old('id_kolam_sewa', @$penyewaan->id_kolam_sewa) == null ? 'required' : '' }}>
                                    <option value="">--Pilih--</option>
                                    @foreach ($kolam as $item)
                                        <option value="{{ $item->id_kolam_sewa }}" {{ old('id_kolam_sewa', @$penyewaan->id_kolam_sewa) == $item->id_kolam_sewa ? 'selected' : '' }}>{{ $item->nama_kolam_sewa }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Pilih Kolam.
                                </div>
                            </div>
                        </div>
                        <div class="row g-3 mt-2">
                            <div class="col-6">
                                <label class="form-label" for="">Nama Penyewa</label>
                                <input class="form-control" name="nama_penyewa" value="{{ old('nama_penyewa', @$penyewaan->nama_penyewa) }}" type="text" required>
                                <div class="invalid-feedback">
                                    Nama Penyewa tidak boleh kosong.
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="form-label" for="">Nomor Handphone</label>
                                <input class="form-control" name="nohp" value="{{ old('nohp', @$penyewaan->nohp) }}" type="number" required>
                                <div class="invalid-feedback">
                                    Nomor Handphone tidak boleh kosong.
                                </div>
                            </div>
                        </div>
                        <div class="row g-3 mt-2">
                            <div class="col-12 mb-3">
                                <label class="form-label" for="">Alamat</label>
                                <textarea class="form-control" name="alamat" id="" cols="10" rows="3" required>{{ old('alamat', @$penyewaan->alamat) }}</textarea>
                                <div class="invalid-feedback">
                                    Alamat tidak boleh kosong.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card mt-2">
                            <div class="card-body">
                                <h6>Data Ikan</h6>
                                <label class="form-label" for="">Pilih Stok</label>

                                <div class="row g-3 mt-2">
                                    <div class="col-3 mb-2"><span>Stok Ikan</span></div>
                                    <div class="mb-2" style="width: 40px"><span>:</span></div>
                                    <select class="form-select" name="id_stok" id="id_stok" required>
                                        <option value="">--Pilih--</option>
                                        @foreach ($stok as $item)
                                            <option value="{{ $item->id_kolam }}" data-isi="{{$item->isi_kolam}}">{{ $item->nama_kolam }} - {{$item->isi_kolam}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="stok" id="stok-ikan" value="{{ $stok_ikan }}">
                                </div>
                                <div class="row g-3 mt-2">
                                    <div class="col-3 mb-2"><span>Harga 1 Ikan</span></div>
                                    <div class="mb-2" style="width: 40px"><span>:</span></div>
                                    <div class="col-4 mb-2"><span>Rp10.000</span></div>
                                </div>
                                @if (!empty($penyewaan))
                                <div class="row g-3 mt-2">
                                    <div class="col-3 mb-2"><span>Jumlah Beli Sebelumnya</span></div>
                                    <div class="mb-2" style="width: 40px"><span>:</span></div>
                                    <div class="col-4 mb-2"><span>{{ @$penyewaan->tangkapan }}</span></div>
                                </div>
                                @endif
                                <div class="row g-3 mt-2">
                                    <div class="col-7">
                                        @if (!empty($penyewaan))
                                        <label class="form-label" for="">Jumlah Beli Sekarang</label>
                                        @else
                                        <label class="form-label" for="">Jumlah Ikan yang Dibeli</label>
                                        @endif
                                        <input type="number" class="form-control" id="beli" name="tangkapan" {{ old('tangkapan', @$penyewaan->tangkapan) != null ? '' : 'required' }}>
                                        <div class="invalid-feedback">
                                            Jumlah Beli tidak boleh kosong.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card mt-2">
                            <div class="card-body">
                                <h6>Data Pembayaran</h6>
                                <div class="row g-3 mt-2">
                                    <div class="col-3 mb-2"><span>Total Bayar</span></div>
                                    <div class="mb-2" style="width: 40px"><span>:</span></div>
                                    <div class="col-4 mb-2">Rp<span id="pembayaranTotal"></span></div>
                                    <input type="hidden" name="total_bayar" id="total_bayar">
                                </div>
                                <div class="row g-3 mt-2">
                                    <div class="col-7">
                                        <label class="form-label" for="">Dibayarkan</label>
                                        <input type="number" class="form-control" name="bayar" id="bayar" {{ old('bayar', @$penyewaan->bayar) != null ? '' : 'required' }}>
                                        <div class="invalid-feedback">
                                            Masukan Nominal yang dibayarkan.
                                        </div>
                                    </div>
                                </div>
                                <div class="row g-3 mt-2">
                                    <div class="col-3 mb-2"><span>Kembali</span></div>
                                    <div class="mb-2" style="width: 40px"><span>:</span></div>
                                    <div class="col-4 mb-2">Rp<span id="pembayaranKembali"></span></div>
                                    <input type="hidden" name="kembali" id="kembalian">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row g-3 mb-5 justify-content-end">
                    <div class="col-4 text-end">
                        <button class="btn btn-primary btn-lg" type="submit">Bayar</button>
                    </div>
                </div>
            </form>
        </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        const forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()


        $(document).ready(function(){


            $('.transaksi').hide()
            $('#status').on('change', function(){
                if(this.value === 'Selesai'){
                    $('.transaksi').show()
                    $('#tangkapan').keyup(function(){
                        const tangkapan = $('#tangkapan').val()
                        const total = tangkapan * 5000;
                        $('#total').html(total)
                        $('#totalbayar').val(total)
                    })
                }else{
                    $('.transaksi').hide()
                }
            })

            let total, bayar, temp, kembali, x, id_sewa, id_kolam, tangkapan, beli, dum;
            const kolam = @json(@$kolam);

            $('#beli').keyup(function() {
                beli = $('#beli').val()
                dum = beli*10000
                console.log(dum)
                let number_string = dum.toString(),
                    sisa = number_string.length % 3,
                    rupiah = number_string.substr(0, sisa),
                    ribuan = number_string.substr(sisa).match(/\d{3}/g);

                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                        let rp = 'Rp' + rupiah;
                    }
                $('#pembayaranTotal').html(rupiah)
                $('#total_bayar').val(dum)

            })

            $('#bayar').keyup(function() {
                bayar = $('#bayar').val()
                temp = bayar-dum
                console.log(temp)

                let number = temp.toString(),
                    sisaa = number.length % 3,
                    rupiahh = number.substr(0, sisaa),
                    ribuann = number.substr(sisaa).match(/\d{3}/g);

                    if (ribuann) {
                        separatorr = sisaa ? '.' : '';
                        rupiahh += separatorr + ribuann.join('.');
                        let rpp = 'Rp' + rupiahh;
                    }
                    $('#pembayaranKembali').html(rupiahh);
                    $('#kembalian').val(temp);
            })

            $('#id_stok').on('change', function () {
                const selected = $(this).find('option:selected');
                const isi = selected.data('isi');
                $('#stok-ikan').val(isi)
            })

            $('#id_kolam_sewa').on('change', function () {
                // console.log(kolam)
                const kolamValue = kolam.find(obj => obj.id_kolam_sewa === parseInt(this.value));
                // $('#stock-ikan').html(kolamValue)
            })
        })
    </script>
@endpush
